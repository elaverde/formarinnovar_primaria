<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$asi=new Models\Asignaciones();
$info= new Models\Info_System();
$result=  new stdClass();
$index=0;
$grados=null;
$asignaciones=[];
$data=null;
$aux1="";
if($info->session_active()){
    $db->extract_dates("GET");
    $data=$asi->list_asignaciones_all_by_grado();


    $result = array();
    foreach ($data as $r) {
        $result[$r['dni']][] = $r;
    }
    $orden = array();
    $i=0;
    foreach ($result as $r) {
        
        $orden[$i]= $r;
        $i+=1;
    }

    echo json_encode($orden);

}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>