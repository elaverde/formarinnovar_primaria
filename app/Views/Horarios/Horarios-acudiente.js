var Horarios = new Vue({
    el: '#Horarios',
    data: {
        id: 0,
        operation: "Agregar Horarios",
        list: null,
        pensum:null,
        mismaterias:null,
        horario: {dia1:null,dia2:null,dia3:null,dia4:null,dia5:null,dia6:null,dia7:null},
        asignaciones: null,
        preload_list: false,
        optionsSelect1:[{text:"Cargando..",value:"0"}],
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        alumnos:null,
        foto:null,
        alumno:null
    },
    created: function () {
        s=this;
        this.get_alumnos();
        //this.
    },
    methods: {
        get_alumnos:function () {
            var url="Services/Acudientes/get_alumnos.php";
            $.get(url, function (data) {
                s.alumnos=data;
                s.set_alumno(data[0]);
            });
        },
        set_alumno:function(data){
            s.alumno_activo={
                fullnombre:data.nombre+" "+data.apellidos,
                foto:data.foto,
                gradoid:data.gradoid,
                grado:data.grado,
                matricula:data.matricula
            };
            s.alumno=s.alumno_activo.fullnombre;
            s.foto=s.alumno_activo.foto;
            s.onListPensum();
        },
        add_grupo:function(add){
            var self=this;
            
            if($("#"+add.id).attr("data-matriculado")=="true"){
                swal({
                    title: '¿Estás seguro?',
                    text: "salirte de este grupo",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, muy seguro!',
                    cancelButtonText: 'No voy mirar otras opciones!',
                }).then((result) => {
                    if (result.value) {
                        save({
                            url: "Services/Grupos/delete.php?id="+$("#"+add.id).attr("data-id")+"&id_asignacion="+add.asignaciones_id+'&idmatricula='+s.alumno_activo.matricula,
                            result: function (data) {
                                if (data.completed) {
                                    swal({
                                        type: 'success',
                                        title: 'Te has eliminado correctamente',
                                        showConfirmButton: false,
                                        timer: 3500
                                    });
                                    
                                    $("#"+add.id).attr("data-matriculado","false");
                                    $("#"+add.id).removeAttr("style");
                                    $("#profesor"+add.id).removeAttr("style");
                                    

                                    s.get_alumnos();
                                }
                                if (data.completed == "expired") {
                                    session_expired();
                                }
                            }
                        });
                    }
                });
            }else{
                swal({
                    title: '¿Estás seguro?',
                    text: "Matricularte en este grupo",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, muy seguro!',
                    cancelButtonText: 'No voy mirar otras opciones!',
                }).then((result) => {
                    if (result.value) {
                        save({
                            url: "Services/Grupos/save.php?horarios_id="+add.id+"&id_asignacion="+add.asignaciones_id+'&idmatricula='+s.alumno_activo.matricula,
                            result: function (data) {
                                if (data.completed) {
                                    swal({
                                        type: 'success',
                                        title: 'Te has matriculado correctamente',
                                        showConfirmButton: false,
                                        timer: 3500
                                    });
                                    s.get_alumnos();
                                }
                                if (data.completed == "expired") {
                                    session_expired();
                                }
                            }
                        });
                    }
                });
            }
        },
        onListmyMatriculas:function(){
            var self=this;
            var url = 'Services/Horarios/list_my_matriculas.php?idmatricula='+s.alumno_activo.matricula;
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.mismaterias=data;
                    var n=self.mismaterias.length;
                    for (var i=0; i<n; i++){
                      
                        $("#"+self.mismaterias[i].horarios_id).css("background-color","#26C6DA");
                        $("#"+self.mismaterias[i].horarios_id).css("cursor","no-drop");
                        $("#profesor"+self.mismaterias[i].horarios_id).css("background-color","#FFF");
                        $("#profesor"+self.mismaterias[i].horarios_id).css("border-radius","10px");
                        $("#profesor"+self.mismaterias[i].horarios_id).css("padding","3px");
                        $("#"+self.mismaterias[i].horarios_id).attr("data-matriculado","true");
                        $("#"+self.mismaterias[i].horarios_id).attr("data-id",self.mismaterias[i].id);
                    }
                }
            });
        },
        onListPensum:function(){
            var self=this;
            var url = 'Services/Pensum/list.php?idmatricula='+s.alumno_activo.matricula;
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.pensum = data;
                    var n=data.length;
                    var id="";
                    for(var i=0; i<n;i++){
                        if(i==(n-1)){
                            id=id+data[i].id_asignacion;
                        }else{
                            id=id+data[i].id_asignacion+",";
                        }
                    }
                    $("#pensumlist").val(id);
                    self.onlist();
                }
            });
        },
        onlist:function(){
            var self=this;
            var url = 'Services/Horarios/list_my_horario.php?lista='+$("#pensumlist").val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.horario=data;
                    self.onListmyMatriculas();
                }
            });
        }
    }
});
