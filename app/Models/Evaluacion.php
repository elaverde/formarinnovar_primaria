<?php

namespace Models;
class Evaluacion
{
	private $id;
	private $ejecucion_plan_id;
	private $id_rubrica;
	private $competencia;
	private $nota;
	private $comentario;
	private $id_plan;
	private $profesor;
	private $dia;
    private $matricula_id;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
   /* public function exists_ejecucion(){
        $sql="SELECT
                COUNT(*) AS Total
            FROM
                ejecucion_plan
            INNER JOIN evaluacion ON evaluacion.ejecucion_plan_id = ejecucion_plan.id
            WHERE
                ejecucion_plan.id_plan = :id_plan
            AND evaluacion.id_rubrica = :id_rubrica";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':id_plan'=>$this->__get('id_plan'),
			':id_rubrica'=>$this->__get('id_rubrica')
        ));
        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
*/
   public function exists_ejecucion(){
        $sql="SELECT  COUNT(*) AS Total FROM
        ejecucion_plan
        INNER JOIN evaluacion ON evaluacion.ejecucion_plan_id = ejecucion_plan.id
        where ejecucion_plan.matricula_id= :matricula_id AND evaluacion.competencia= :competencia and  ejecucion_plan.id_plan= :id_plan";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':competencia'=>$this->__get('competencia'),
			':id_plan'=>$this->__get('id_plan'),
        ));
        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
    public function list(){
        //generamos la consulta
        $sql="SELECT
                evaluacion.id,
                evaluacion.ejecucion_plan_id,
                evaluacion.id_rubrica,
                evaluacion.competencia,
                evaluacion.nota,
                evaluacion.comentario,
                evaluacion.profesor,
                evaluacion.dia,
                ejecucion_plan.matricula_id,
                ejecucion_plan.id_plan
            FROM
                ejecucion_plan
            INNER JOIN evaluacion ON evaluacion.ejecucion_plan_id = ejecucion_plan.id
            WHERE
                ejecucion_plan.matricula_id = :matricula_id
            AND ejecucion_plan.id_plan = :id_plan";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':id_plan'=>$this->__get('id_plan')
        ));
        //buscamos los n resultados
        $result=$resultado1;
        return $result;
    }
    public function total_evaluacion(){
        $sql="SELECT
            COUNT(*) total
        FROM
            ejecucion_plan
        WHERE
            ejecucion_plan.matricula_id = :matricula_id
        AND promediorubrica <> ''";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id')
        ));
        return $resultado[0]["total"];
    }
    public function save(){
        $sql="INSERT INTO evaluacion (id, ejecucion_plan_id, id_rubrica, competencia, nota, comentario, profesor, dia) VALUES (:id, :ejecucion_plan_id, :id_rubrica, :competencia, :nota, :comentario, :profesor, :dia)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':ejecucion_plan_id'=>$this->__get('ejecucion_plan_id'),
			':id_rubrica'=>$this->__get('id_rubrica'),
			':competencia'=>$this->__get('competencia'),
			':nota'=>$this->__get('nota'),
			':comentario'=>$this->__get('comentario'),
			':profesor'=>$this->__get('profesor'),
			':dia'=>$this->__get('dia')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE evaluacion SET  nota = :nota, comentario = :comentario , profesor = :profesor , dia = :dia WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':nota'=>$this->__get('nota'),
            ':comentario'=>$this->__get('comentario'),
            ':profesor'=>$this->__get('profesor'),
			':dia'=>$this->__get('dia')
        ));
        return $resultado;
    }
    public function exists_evaluacion_by_rubrica(){
        $sql="SELECT COUNT(*) as Total
        FROM
        evaluacion WHERE evaluacion.id_rubrica = :id_rubrica" ;

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':id_rubrica'=>$this->__get('id_rubrica')
        ));

        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
    
    public function delete(){
        $sql="DELETE FROM evaluacion WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}