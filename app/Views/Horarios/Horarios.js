elements = {
    
	"#profesores_id": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Profesor"
	},
	"#asignacion": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Asignación"
	},
	"#periodo": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Periodo"
	},
	"#activo": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Activo"
	},
	"#hora_inicio": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Hora inicio"
	},
	"#hora_final": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Hora Final"
	},
	"#dia": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Día"
	}
}
var Horarios = new Vue({
    el: '#Horarios',
    data: {
        id: 0,
        operation: "Agregar Horarios",
        list: null,
        horario: {dia1:null,dia2:null,dia3:null,dia4:null,dia5:null,dia6:null,dia7:null},
        asignaciones: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
    },
    created: function () {
        this.onlistProfesores(1);
        this.onlist();
        this.onlistAsignaciones();
    },
    methods: {
        
        
        delted_item: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Horarios/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onSetProfesor:function(data){
            console.log(data);
            $("#profesores_id").val(data.id);
            $('#b_profesores').modal('toggle');
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Horarios";
            $("#profesores_id").val(data.profesores_id);
			$("#asignacion").val(data.asignacion);
			$("#periodo").val(data.periodo);
			$("#activo").val(data.activo);
			$("#hora_inicio").val(data.hora_inicio);
			$("#hora_final").val(data.hora_final);
			$("#dia").val(data.dia);
        },
        onlistAsignaciones: function (page) {
            var self=this;
            var url = 'Services/Asignaciones/list_asignaciones_all_by_grado.php'
            $.get(url, function (data) {
                console.log(data);
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.asignaciones=data;
                }
            });
        },
        onlistProfesores: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Profesores/list.php?paginas=15&page=' + page + '&nombre=' + $('#search').val() + '&apellidos=' + $('#search').val() + '&identificacion=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onlist:function(){
            var self=this;
            var url = 'Services/Horarios/list.php';
            $.get(url, function (data) {
                console.log(data.dia7);
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.horario=data;
                }
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    url: "Services/Horarios/save.php?" + $("#form_Horarios").serialize(),
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Horarios se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                        $("#profesores_id").val("");
						$("#hora_inicio").val("");
						$("#hora_final").val("");
						$("#dia").val("");
			            self.onlist(1);
                        self.operation = "Agregar Horarios";
                    }
                });
            }
        }
    }
});
$(function(){
    $('#asignacion').multiSelect({
        selectableHeader: "<input id='b' type='text' class='form-control fi_b' autocomplete='off' placeholder='Buscar'>",
        selectionHeader: "<input type='text' class='form-control fi_b' autocomplete='off' placeholder='Buscar'>",
        afterInit: function(ms){
          var that = this,
              $selectableSearch = that.$selectableUl.prev(),
              $selectionSearch = that.$selectionUl.prev(),
              selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
              selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
      
          that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
          .on('keydown', function(e){
            if (e.which === 40){
              that.$selectableUl.focus();
              return false;
            }
          });
      
          that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
          .on('keydown', function(e){
            if (e.which == 40){
              that.$selectionUl.focus();
              return false;
            }
          });
        },
        afterSelect: function(){
          this.qs1.cache();
          this.qs2.cache();
        },
        afterDeselect: function(){
          this.qs1.cache();
          this.qs2.cache();
        }
      });
    setTimeout(function(){
       $('#asignacion').multiSelect('refresh');
    },1000);
});