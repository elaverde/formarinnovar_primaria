<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$asi=new Models\Asignaciones();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$asi->__set("grado_id",$grado_id);
    echo json_encode($asi->list_select_by_grado());
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>