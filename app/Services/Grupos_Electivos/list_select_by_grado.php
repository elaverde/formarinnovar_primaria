<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$gru=new Models\Grupos_Electivos();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $gru->__set("grado_id",$grado_id);
    $grupos=$gru->list_grupo_select();
    $array[]=array(
        "id" => "0",
        "grado_id" => "0",
        "nombre"=>"No grupo"
    );
    
    foreach ( $grupos as $valor) {
        $array[]=$valor;
    }
    echo json_encode($array);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>