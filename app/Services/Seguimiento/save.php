<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$seg=new Models\Seguimiento();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $data=$_GET["notas"];
    $index=0;
    foreach ( $_GET["id"] as $dni ) { 
        $seg->__set("id",$dni);
        $seg->__set("dia", date("Y-m-d H:i:s"));
        $seg->__set("profesor", $_SESSION['fi_id']);
        $seg->__set("desempeno", $_GET["notas"][$index]);
        $seg->update();
        $index++;
    }
    $result->completed =true;
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>