<?php

namespace Models;
class Plan
{
	private $id;
	private $asignaciones_id;
	private $derechos;
	private $aprendizaje;
	private $unidad;
	private $unidad_tematica;
    private $n;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO plan (id, asignaciones_id, derechos, aprendizaje, unidad, unidad_tematica, n) VALUES (:id, :asignaciones_id, :derechos, :aprendizaje, :unidad, :unidad_tematica, :n)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':asignaciones_id'=>$this->__get('asignaciones_id'),
			':derechos'=>$this->__get('derechos'),
            ':aprendizaje'=>$this->__get('aprendizaje'),
            ':unidad'=>$this->__get('unidad'),
            ':unidad_tematica'=>$this->__get('unidad_tematica'),
            ':n'=>$this->__get('n')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE plan SET asignaciones_id= :asignaciones_id, derechos= :derechos, aprendizaje= :aprendizaje, unidad = :unidad, unidad_tematica = :unidad_tematica, n = :n WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':asignaciones_id'=>$this->__get('asignaciones_id'),
            ':derechos'=>$this->__get('derechos'),
            ':aprendizaje'=>$this->__get('aprendizaje'),
            ':unidad'=>$this->__get('unidad'),
            ':unidad_tematica'=>$this->__get('unidad_tematica'),
            ':n'=>$this->__get('n')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM plan WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new\stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT 
            p.id,
            p.asignaciones_id,
            p.derechos,
            p.aprendizaje,
            p.unidad,
            p.unidad_tematica,
            p.n,
            a.grado_id
        FROM plan p, asignaciones a WHERE (p.asignaciones_id = :asignaciones_id and p.asignaciones_id = a.id ) and (p.derechos LIKE :derechos OR  p.aprendizaje LIKE :aprendizaje) LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':asignaciones_id'=>$this->__get('asignaciones_id'),
			':derechos'=>'%'.$this->__get('derechos').'%',
			':aprendizaje'=>'%'.$this->__get('aprendizaje').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM plan p, asignaciones a WHERE (p.asignaciones_id = :asignaciones_id and p.asignaciones_id = a.id ) and (p.derechos LIKE :derechos OR  p.aprendizaje LIKE :aprendizaje) ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':asignaciones_id'=>$this->__get('asignaciones_id'),
			':derechos'=>'%'.$this->__get('derechos').'%',
			':aprendizaje'=>'%'.$this->__get('aprendizaje').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}