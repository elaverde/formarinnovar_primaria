<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$hor=new Models\Horarios();
$gru=new Models\Grupos();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $mylista=explode(",",$lista);
    $data=$hor->list_my_horario($mylista);
    $result = array();
    foreach ($data as $r) {
        $gru->__set("horarios_id",$r["id"]);
        $r["cupos"]=$gru->get_cupos();
        $result["dia".$r['dia']][] =$r;
    }
   echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>