<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$gru=new Models\Grupos();
$pen=new Models\Pensum();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $gru->__set("id",$id);
    $result->completed=boolval($gru->delete());
    if($result->completed ){
        if (!isset($idmatricula)) {
            //si no esta definidad es el perfil estudiante//
            $idmatricula=$_SESSION['fi_id_matricula'];
        }



        $gru->__set('matricula_id',$idmatricula);
        $gru->__set('id_asignacion',$id_asignacion);
        $data=$gru->get_horas_matriculadas();
        $pen->__set('matricula_id',$idmatricula);
        $pen->__set('id_asignacion',$id_asignacion);
        if(is_null($data)){
            $data=0;
        }
        $pen->__set('tiempo',$data);
        $pen->update_hora_catedra();
        echo json_encode($result);
    }else{
        $result->completed=false; 
        echo json_encode($result);  
    }
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>