<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$eva=new Models\Evaluacion();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $eva->__set("matricula_id",$matricula_id);
    $data=$eva->total_evaluacion();
    $result->count = $data;
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>