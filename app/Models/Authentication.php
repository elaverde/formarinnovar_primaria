<?php

/**
 * Created by PhpStorm.
 * User: elaverde
 * Date: 21/8/2018
 * Time: 21:52
 */
namespace Models;
class Authentication
{
    private $pass;
    private $dni;
    private $con;
    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function access($level){
        if($level==1){
            $sql="SELECT * FROM alumnos WHERE identificacion = :dni and password = :pass";
            $usertype="alumno";
        }
        if($level==2){
            $sql="SELECT * FROM administrador WHERE identificacion = :dni and password = :pass";
            $usertype="admin";
        }
        if($level==3){
            $sql="SELECT * FROM profesores WHERE identificacion = :dni and password = :pass";
            $usertype="profesor";
        }
        if($level==4){
            $sql="SELECT * FROM acudiente WHERE identificacion = :dni and password = :pass";
            $usertype="acudiente";
        }
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':pass'=>md5($this->__get("pass")),
            ':dni'=>$this->__get("dni"),
        ));
        if(count($resultado)==0){
            return false;
        }else{
            $_SESSION['fi_name']=$resultado[0]["nombre"];
            $_SESSION['fi_lastname']=$resultado[0]["apellidos"];
            $_SESSION['fi_id']=$resultado[0]["id"];
            $_SESSION['fi_foto']=$resultado[0]["foto"];
            $_SESSION['fi_email']=$resultado[0]["email"];
            $_SESSION['fi_type']=$usertype;
             $data="ID: ".$resultado[0]["id"]." Nombre: ".$resultado[0]["nombre"]." Apellidos: ".$resultado[0]["apellidos"]." Hora: ".date("Y-m-d  h:i:s")."\r\n";
            error_log($data, 3, "../../Logs/log.log");
            if($level==1){ 
                $this->info_alumno($resultado[0]["id"]);
            }
            return true;
        }
    }
    public function info_alumno($id){
        $sql="SELECT
            matricula.id,
            matricula.matriculador,
            matricula.alumnos_id,
            matricula.grado_id,
            matricula.periodo,
            matricula.activo,
            matricula.fecha,
            grado.nombre
        FROM
            grado
        INNER JOIN matricula ON matricula.grado_id = grado.id
        INNER JOIN alumnos ON matricula.alumnos_id = alumnos.id
        WHERE
            matricula.activo = 1
        AND alumnos.id = ".$id;
        $resultado = $this->con->ejecutarConsulta($sql);
        if(count($resultado)==0){
            return false;
        }else{
            $_SESSION['fi_id_matricula']=$resultado[0]["id"];
            $_SESSION['fi_matriculador']=$resultado[0]["matriculador"];
            $_SESSION['fi_grado_id']=$resultado[0]["grado_id"];
            $_SESSION['fi_grado_nombre']=$resultado[0]["nombre"];
            $_SESSION['fi_periodo']=$resultado[0]["periodo"];
            $_SESSION['fi_fecha']=$resultado[0]["fecha"];
        }
    }
    public function generate_pass($longitud = 6, $opcLetra = TRUE, $opcNumero = TRUE, $opcMayus = TRUE, $opcEspecial = FALSE){
        $letras ="abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales ="|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == TRUE) $listado .= $letras;
        if ($opcNumero == TRUE) $listado .= $numeros;
        if($opcMayus == TRUE) $listado .= $letrasMayus;
        if($opcEspecial == TRUE) $listado .= $especiales;

        for( $i=1; $i<=$longitud; $i++) {
            $caracter = $listado[rand(0,strlen($listado)-1)];
            $password.=$caracter;
            $listado = str_shuffle($listado);
        }
        return $password;
    }
    public function recovery(){

        $sql="SELECT * FROM usuarios WHERE correo=:correo";
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':correo'=>$this->__get("email")
        ));
        //correo no existe//
        if(count($resultado)==0){
            $recover["recover"]=false;
            return  $recover;
        }else{
            $pass=$this->generate_pass();
            //se modifica el password//
            $sql="UPDATE usuarios SET  contrasena= :contrasena WHERE correo=:correo";
            $r = $this->con->ejecutarActualizacion($sql,array(
                ':contrasena'=>md5($pass),
                ':correo'=>$this->__get("email"),
            ));
            $recover["info"]=$r;
            $recover["last_name"]=$resultado[0]["apellidos"];
            $recover["email"]=$resultado[0]["correo"];
            $recover["name"]=$resultado[0]["nombres"];
            $recover["pass"]=$pass;
            $recover["recover"]=true;
            return  $recover;
        }
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }
}