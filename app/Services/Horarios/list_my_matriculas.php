<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$hor=new Models\Horarios();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    //es el id de la matricula para trer id matriculados de horarios//
    if (!isset($idmatricula)) {
        //si no esta definidad es el perfil estudiante//
        $idmatricula=$_SESSION['fi_id_matricula'];
    }
    $hor->__set("id",$idmatricula);
    $data=$hor->list_my_matriculas();
    echo json_encode($data);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>