<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$acu=new Models\Acudientes();
$info= new Models\Info_System();

$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $acu->__set("id",$_SESSION['fi_id']);
    echo json_encode($acu->get_alumnos());
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>