<?php
require_once "Config/Autoload.php";
Config\Autoload::run();
$info=new Models\Info_System();
if(!$info->session_active()){
    header("Location:index.html");
}
?>
<!DOCTYPE html>
<html id="main-app" lang="en">
<!-- Mirrored from wrappixel.com/demos/admin-templates/material-pro/materplataforma.phphtml by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Aug 2018 17:37:45 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Formar Innovar app</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <link href="css/app.css" id="theme2" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/multi-select.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
</head>
<body class="fix-header fix-sidebar card-no-border">
    <input type="hidden" id="fi_id" name="fi_id" value="<?= $_SESSION['fi_id'] ?>">
    <input type="hidden" id="fi_lastname" name="fi_lastname" value="<?= $_SESSION['fi_lastname'] ?>">
    <input type="hidden" id="fi_name" name="fi_name" value="<?= $_SESSION['fi_name'] ?>">
    <input type="hidden" id="fi_type" name="fi_type" value="<?= $_SESSION['fi_type'] ?>">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
    <img class="circular" width="100" height="100" src="images/preload.svg">
      <!--  <svg class="circular" viewBox="25 25 50 50">
           <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>-->
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="images/logo.svg" width ="50"alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="images/logo.svg" width="50" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                            <!-- dark Logo text -->
                            <img src="assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo text -->
                            <img src="assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                                href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box">
                            <!--<a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i
                                    class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i
                                        class="ti-close"></i></a> </form>-->
                        </li>
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <!--
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="#"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new
                                                        admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that
                                                        you have event</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this
                                                        template as you want</span> <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all
                                                notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                            -->
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <!-- <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" id="2"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-email"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox dropdown-menu-right scale-up" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">You have 4 new messages</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <a href="#">
                                                <div class="user-img"> <img src="assets/images/users/1.jpg" alt="user"
                                                        class="img-circle"> <span class="profile-status online pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="user-img"> <img src="assets/images/users/2.jpg" alt="user"
                                                        class="img-circle"> <span class="profile-status busy pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See
                                                        you at</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="user-img"> <img src="assets/images/users/3.jpg" alt="user"
                                                        class="img-circle"> <span class="profile-status away pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span>
                                                    <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="user-img"> <img src="assets/images/users/4.jpg" alt="user"
                                                        class="img-circle"> <span class="profile-status offline pull-right"></span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                                    <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all
                                                e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                            -->
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><img src="<?= $_SESSION['fi_foto'] ?>"  alt="user"
                                    class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?= $_SESSION['fi_foto'] ?>"  alt="user"></div>
                                            <div class="u-text">
                                                <h4>
                                                    <?= $_SESSION['fi_name'] . " " . $_SESSION['fi_lastname'] ?>
                                                </h4>
                                                <p class="text-muted">
                                                    <?= $_SESSION['fi_email'] ?>
                                                </p><a data-ng-modules="add_perfil"  href="#add_perfil" class="btn btn-rounded btn-danger btn-sm">Ver
                                                    Perfil</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a data-ng-modules="add_perfil"  href="#add_perfil" ><i class="ti-user"></i> Mi perfil</a></li>
                                    <li><a href="logout.php"><i class="fa fa-power-off"></i> Salir</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <div class="dropdown-menu dropdown-menu-right scale-up"> <a class="dropdown-item" href="#"><i
                                        class="flag-icon flag-icon-in"></i> India</a> <a class="dropdown-item" href="#"><i
                                        class="flag-icon flag-icon-fr"></i> French</a> <a class="dropdown-item" href="#"><i
                                        class="flag-icon flag-icon-cn"></i> China</a> <a class="dropdown-item" href="#"><i
                                        class="flag-icon flag-icon-de"></i> Dutch</a> </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url(assets/images/background/user-info.jpg) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img height="50" src="<?= $_SESSION['fi_foto'] ?>" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="true">
                            <?= $_SESSION['fi_name'] . " " . $_SESSION['fi_lastname'] ?></a>
                        <div class="dropdown-menu animated flipInY">
                            <a data-ng-modules="add_perfil"  href="#add_perfil" class="dropdown-item"><i class="ti-user"></i> Mi perfil</a>
                            <div class="dropdown-divider"></div> 
                            <a href="pages-login.html" class="dropdown-item">
                                <i class="fa fa-power-off"></i> Salir
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">Un lugar para ser feliz</li>
                        <?php if($info->get_type_user("admin")) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-university"></i><span class="hide-menu">
                             Alumnos</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a data-ng-modules="add_alumnos" class="active" href="#add_alumnos">
                                    <i  class="fas fa-graduation-cap"></i>
                                        Registro Alumno</a>
                                </li>
                                <li>
                                    <a data-ng-modules="add_acudiente" class="active" href="#add_acudiente">
                                    <i  class="far fa-address-card"></i>
                                        Registro Acudiente</a>
                                </li>
                                <li>
                                    <a data-ng-modules="add_matricula" class="active" href="#add_matricula">
                                    <i  class="far fa-address-card"></i>
                                        Matricula</a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>    
                        <?php if($info->get_type_user("admin") or $info->get_type_user("profesor") ) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="far fa-object-ungroup"></i><span class="hide-menu">
                             Diseño Curricular</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <?php if($info->get_type_user("admin") ) { ?>
                                <li >
                                    <a data-ng-modules="add_grados" href="#add_grados" class="active">
                                        <i class="fab fa-gitter"></i> Grados
                                    </a>
                                </li>
                                <?php } ?>
                                <?php if($info->get_type_user("admin") ) { ?>
                                <li >
                                    <a data-ng-modules="add_grupos" href="#add_grupos" class="active">
                                        <i class="fas fa-users"></i> Grupos
                                    </a>
                                </li>
                                <li >
                                    <a data-ng-modules="add_asignacion" href="#add_asignaciones" class="active">
                                        <i class="fas fa-book"></i> Asignaciones
                                        </a>
                                </li>
                                <?php } ?>
                                <li >
                                    <a data-ng-modules="add_plan" href="#add_plan" class="active">
                                        <i class="fas fa-tasks"></i> Plan de ejecución
                                        </a>
                                </li>
                                <!--<li>
                                    <a data-ng-modules="add_asignar_catedra" href="#add_asignar_catedra" class="active">
                                        <i class="fas fa-align-justify"></i>
                                         Asignar Cátedra
                                    </a>
                                </li>-->
                            </ul>
                        </li>
                        <?php } ?>  
                        <?php if($info->get_type_user("admin") or $info->get_type_user("profesor") ) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fab fa-wpforms"></i><span class="hide-menu">
                            Mis rúbricas</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li >
                                    <a data-ng-modules="add_mis_rubricas" href="#add_mis_rubricas" class="active">
                                        <i class="fas fa-tasks"></i> Ejecución 
                                        </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?> 
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="far fa-calendar-alt"></i> <span class="hide-menu">
                            Horarios</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <?php if($info->get_type_user("admin")) {?>
                                    <li><a data-ng-modules="add_horario" href="#add_horario" class="active"><i class="fas fa-address-book"></i> Crear horarios</a></li> 
                                <?php } ?>
                                <?php if($info->get_type_user("alumno") ) {?>
                                    <li><a id="add_horario_alumno" data-ng-modules="add_horario_alumno" href="#add_horario_alumno" class="active"><i class="fas fa-address-book"></i> Crear mi horario</a></li> 
                                <?php } ?>
                                <?php if($info->get_type_user("acudiente") ) {?>
                                    <li><a id="add_horario_acudiente" data-ng-modules="add_horario_acudiente" href="#add_horario_acudiente" class="active"><i class="fas fa-address-book"></i> Crear mi horario</a></li> 
                                <?php } ?>
                            </ul>
                        </li>
                        <?php if($info->get_type_user("acudiente") ) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-users"></i><span class="hide-menu">
                             Mis notas</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a data-ng-modules="get_calificaciones" href="#get_calificaciones" class="active"><i class="fas fa-address-book"></i> Calificaciones</a></li> 
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if( $info->get_type_user("alumno") ) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-users"></i><span class="hide-menu">
                             Mis notas</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a data-ng-modules="get_calificaciones_alumno" href="#get_calificaciones_alumno" class="active"><i class="fas fa-address-book"></i>Mis Calificaciones</a></li> 
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if($info->get_type_user("admin")) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-users"></i><span class="hide-menu">
                             Profesores</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a data-ng-modules="add_profesor" href="#add_profesor" class="active"><i class="fas fa-address-book"></i> Profesores</a></li> 
                            </ul>
                        </li>
                        <?php } ?>
                        <?php  ?>
                        <?php if($info->get_type_user("admin")) {?>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-users"></i> <span class="hide-menu">
                            Usuarios</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a data-ng-modules="add_user" href="#add_user" class="active"><i class="fas fa-address-book"></i> Usuarios</a></li> 
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <!--<a href="#" class="link" data-toggle="tooltip" title="Perfil"><i class="ti-settings"></i></a>
                <!-- item-->
                <!--<a href="#" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <!-- item-->
                <a href="#" style="float:right;" class="link" data-toggle="tooltip" title="Salir"><i class="mdi mdi-power"></i></a>
            </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                            class="ti-settings text-white"></i></button>
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">{{module}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Formar Innovar</a></li>
                            <li class="breadcrumb-item active">{{module}}</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div style="display: flex !important; align-items: center !important; justify-content: center !important;"
                    class="row">
                    <img v-if="preload" width="100" height="100" src="images/preload.svg">
                    <div v-if="!preload" id="vistas" class="col-12">
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Panel de servicio <span><i class="ti-close right-side-toggle"></i></span>
                        </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>Temas</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">{{app_name}} </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/popper/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="js/jasny-bootstrap.js"></script>
    <script src="js/vue.js"></script>
    <script src="assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="assets/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.3.5/sweetalert2.all.js"></script>
    <script src="js/main.js?v=0.1"></script>
    <script src="js/jquery.multi-select.js"></script>
    <script src="js/jquery.quicksearch.js"></script>
    <script src="js/Atlas.js?v=0.01"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
<!-- Mirrored from wrappixel.com/demos/admin-templates/material-pro/materplataforma.phphtml by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Aug 2018 17:37:45 GMT -->
</html>