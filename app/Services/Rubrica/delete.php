<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$rub=new Models\Rubrica();
$eva=new Models\Evaluacion();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $eva->__set("id_rubrica",$id);
    if(!$eva->exists_evaluacion_by_rubrica()){
        $rub->__set("id",$id);
        $result->completed=boolval($rub->delete());
    }else{
        $result->completed=false;
    }
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>