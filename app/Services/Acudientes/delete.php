<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$acu=new Models\Acudientes();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $acu->__set("id",$id);
    $result->completed=boolval($acu->delete());
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>