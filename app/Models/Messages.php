<?php
/**
 * Created by PhpStorm.
 * User: Edilson Laverde
 * Date: 31/05/2017
 * Time: 3:11 PM
 */
//namespace Models;
require '../../PHPMailer/PHPMailerAutoload.php';
class Messages
{

    private $email;
    private $pass;
    private $name;
    private $namefrom;


    private $para;
    private $asunto;
    private $msg;
    private $con;
    private $mail;
    private $platilla;




    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->mail = new PHPMailer();
    }

    public function send(Messages $msg){
        //mail

        $this->mail->SetLanguage("es", "PHPMailer/language/");
        $this->mail->CharSet = 'UTF-8';
        //Tell PHPMailer to use SMTP
        $this->mail->isSMTP();
        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $this->mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $this->mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        //$this->mail->Host = 'smtp.gmail.com';
		$this->mail->Host = 'mail.formarinnovar.com';
       // if($msg->getFile()!=null){
             // attachment
        //}
        $this->mail->Timeout = 60;
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6
        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        //$this->mail->Port = 587;
		$this->mail->Port = 587;
        //Set the encryption system to use - ssl (deprecated) or tls
        $this->mail->SMTPSecure = 'tls';
        //Whether to use SMTP authentication
        $this->mail->SMTPAuth = true;
        //Username to use for SMTP authentication - use full email address for gmail
        $this->mail->Username = $msg->getEmail();
        //Password to use for SMTP authentication
        $this->mail->Password = $msg->getPass();
        //Set who the message is to be sent from
        $this->mail->setFrom($msg->getEmail(), $msg->getNamefrom());
        $this->mail->AddAttachment($msg->getFile());


        //Set who the message is to be sent to
        $this->mail->addAddress($msg->getPara(), $msg->getName());
        //Set the subject line
        $this->mail->Subject = $msg->getAsunto();
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $this->mail->msgHTML($msg->getMsg(), dirname(__FILE__));
        $this->mail->AltBody = 'START FITNESS';

        //sleep(10);
        if (!$this->mail->send()) {
            return "fail" ;
           // return "fail" . $mail->ErrorInfo;
        } else {
            return "OK";
        }
        $this->mail->clearAddresses();

    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }
    public function getFile()
    {
        return $this->file_doc;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getPara()
    {
        return $this->para;
    }

    /**
     * @param mixed $para
     */
    public function setPara($para)
    {
        $this->para = $para;
    }

    /**
     * @return mixed
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * @param mixed $asunto
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;
    }

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getNamefrom()
    {
        return $this->namefrom;
    }

    /**
     * @param mixed $namefrom
     */
    public function setNamefrom($namefrom)
    {
        $this->namefrom = $namefrom;
    }

    public function setFile($file_doc)
    {
       $this->file_doc=$file_doc;
    }

}
