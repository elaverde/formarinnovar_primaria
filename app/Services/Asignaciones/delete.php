<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$asi=new Models\Asignaciones();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $asi->__set("id",$id);
    $result->completed=boolval($asi->delete());
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>