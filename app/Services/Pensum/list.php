<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pen=new Models\Pensum();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    if (isset($idmatricula)) {
        //si esta definidad es el perfil acudiente//
        $pen->__set("matricula_id", $idmatricula);
    }else{
        $pen->__set("matricula_id", $_SESSION['fi_id_matricula']);
    }
    echo json_encode($pen->onlist());
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>