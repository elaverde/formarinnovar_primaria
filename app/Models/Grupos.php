<?php
namespace Models;
class Grupos
{
	private $id;
	private $matricula_id;
    private $horarios_id;
    private $id_asignacion;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function get_cupos(){
        $sql="SELECT COUNT(*) As Total FROM matricula_has_horarios WHERE matricula_has_horarios.horarios_id=:horarios_id";
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':horarios_id'=>$this->__get("horarios_id")
        ));
        return $resultado[0]["Total"];
    }
    public function save(){
        $sql="INSERT INTO matricula_has_horarios (id, matricula_id, horarios_id) VALUES (:id, :matricula_id, :horarios_id)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':matricula_id'=>$this->__get('matricula_id'),
			':horarios_id'=>$this->__get('horarios_id')
        ));
        return $resultado;
    }
    public function get_horas_matriculadas(){
        $sql="SELECT
                    SUM(
                        TIME_TO_SEC(
                            TIMEDIFF(hora_final, hora_inicio)
                        ) / 3600
                    ) AS hours_difference
                FROM
                    horarios
                INNER JOIN matricula_has_horarios ON matricula_has_horarios.horarios_id = horarios.id
                INNER JOIN disponibles ON disponibles.horarios_id = horarios.id
            WHERE
                matricula_has_horarios.matricula_id = :matricula_id
                AND disponibles.asignaciones_id = :id_asignacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':id_asignacion'=>$this->__get('id_asignacion')
        ));
        return $resultado[0]["hours_difference"];
    }
    public function update(){
        $sql="UPDATE matricula_has_horarios SET matricula_id= :matricula_id, horarios_id= :horarios_id WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':matricula_id'=>$this->__get('matricula_id'),
			':horarios_id'=>$this->__get('horarios_id')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM matricula_has_horarios WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM matricula_has_horarios WHERE id LIKE :id LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':id'=>'%'.$this->__get('id').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM matricula_has_horarios WHERE id LIKE :id ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':id'=>'%'.$this->__get('id').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;
        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}