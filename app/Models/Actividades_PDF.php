<?php
namespace Models;
class Actividades_PDF
{
	private $id;
	private $plan_id;
	private $nombre;
	private $url;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO actividades_pdf(id, plan_id, nombre, url) VALUES (:id, :plan_id, :nombre, :url)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':plan_id'=>$this->__get('plan_id'),
			':nombre'=>$this->__get('nombre'),
			':url'=>$this->__get('url')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE actividades_pdf SET nombre = :nombre WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM actividades_pdf WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id')
        ));
        return $resultado;
    }
    public function list(){
        $sql="SELECT * FROM actividades_pdf WHERE plan_id = :plan_id AND nombre like :nombre ";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id'),
			':nombre'=>"%".$this->__get('nombre')."%"
	    ));
        return $resultado;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}