<?php

namespace Models;
class Seguimiento
{
	private $id;
	private $ejecucion_plan_id;
	private $plan_id;
	private $id_activity;
	private $dia;
	private $profesor;
	private $tema;
	private $desempeno;
    private $matricula_id;
    private $id_plan;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO seguimiento (id, ejecucion_plan_id, id_activity, dia, profesor, tema, desempeno) VALUES (:id, :ejecucion_plan_id, :id_activity, :dia, :profesor, :tema, :desempeno)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':ejecucion_plan_id'=>$this->__get('ejecucion_plan_id'),
			':id_activity'=>$this->__get('id_activity'),
			':dia'=>$this->__get('dia'),
			':profesor'=>$this->__get('profesor'),
			':tema'=>$this->__get('tema'),
			':desempeno'=>$this->__get('desempeno')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE seguimiento SET dia= :dia, profesor= :profesor, desempeno= :desempeno WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':dia'=>$this->__get('dia'),
			':profesor'=>$this->__get('profesor'),
			':desempeno'=>$this->__get('desempeno')
        ));
        return $resultado;
    }
    public function exists_seguimiento(){
        $sql="SELECT COUNT(*) as Total
                FROM
                seguimiento
              INNER JOIN ejecucion_plan ON seguimiento.ejecucion_plan_id = ejecucion_plan.id
                where
              seguimiento.id_activity = :id_activity and
              ejecucion_plan.id_plan = :plan_id";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id'),
			':id_activity'=>$this->__get('id_activity')
        ));
        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
    public function exists_seguimiento_by_activity(){
        $sql="SELECT COUNT(*) as Total
                FROM
                seguimiento
              INNER JOIN ejecucion_plan ON seguimiento.ejecucion_plan_id = ejecucion_plan.id
                where
              seguimiento.id_activity = :id_activity";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':id_activity'=>$this->__get('id_activity')
        ));

        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
    public function total_seguimiento(){
        $sql="SELECT
                COUNT(*) total
            FROM
                seguimiento
            INNER JOIN ejecucion_plan ON seguimiento.ejecucion_plan_id = ejecucion_plan.id
            WHERE
                ejecucion_plan.matricula_id = :matricula_id
            AND seguimiento.desempeno <> '' ";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id')
        ));
        return $resultado[0]["total"];
    }
    public function delete(){
        $sql="DELETE FROM seguimiento WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list(){
        $sql="SELECT
            seguimiento.id,
            seguimiento.ejecucion_plan_id,
            seguimiento.id_activity,
            seguimiento.dia,
            seguimiento.profesor,
            seguimiento.tema,
            seguimiento.desempeno,
            ejecucion_plan.matricula_id,
            ejecucion_plan.id_plan
        FROM
            seguimiento
        INNER JOIN ejecucion_plan ON seguimiento.ejecucion_plan_id = ejecucion_plan.id
        WHERE
            ejecucion_plan.matricula_id = :matricula_id
        AND ejecucion_plan.id_plan = :id_plan";

        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':id_plan'=>$this->__get('id_plan')
        ));
        $result=$resultado1;
        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}