<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$access=new Models\Authentication();
$result=  new stdClass();
$db->extract_dates("GET");
$access->__set("dni",$dni);
$access->__set("pass",$pass);
$a1=$access->access(1);
if(!$a1){
    $a2=$access->access(2);
}
if(!$a2 || !$a1 ){
    $a3=$access->access(3);
}
if(!$a3 || !$a2 || !$a1 ){
    $a4=$access->access(4);
}
if($a1 || $a2 || $a3 || $a4){
    $result->access=true;
}else{
    $result->access=false;
}
echo json_encode($result);
?>