<?php

namespace Models;
class Disponibilidad
{
	private $id;
	private $horarios_id;
	private $asignaciones_id;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO disponibles (id, horarios_id, asignaciones_id) VALUES (:id, :horarios_id, :asignaciones_id)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':horarios_id'=>$this->__get('horarios_id'),
			':asignaciones_id'=>$this->__get('asignaciones_id')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM disponibles WHERE disponibles.horarios_id = :horarios_id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':horarios_id'=>$this->__get("horarios_id")
        ));
        return $resultado;
    }

    
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}