elements = {

    "#foto": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Foto"
    },
    "#nombre": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Nombres"
    },
    "#apellidos": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Apellidos"
    },
    "#fecha_nacimiento": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Fecha de nacimiento"
    },
    "#identificacion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Identificación"
    },
    "#tipo_identificacion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo de identificación"
    },
    "#sexo": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Sexo"
    },
    "#direccion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Dirección"
    },
    "#password": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Password"
    },
    "#tipo": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo"
    },
    "#telefono": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Teléfono"
    },
    "#tipo_sangre": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo de sangre"
    },
    "#sede": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Sede"
    },
    "#eps": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Eps"
    },
    "#prepagada": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Prepagada"
    },
    "#email": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Correo eletrónico"
    }
}
var Alumnos = new Vue({
    el: '#Alumnos',
    data: {
        id: 0,
        operation: "Agregar Alumnos",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        optionsSelect1:[{text:"Cargando..",value:"0"}],
        optionsSelect2:[{text:"Cargando..",value:"0"}],
        optionsSelect2aux:[{text:"Cargando..",value:"0"}]
    },
    created: function () {
        this.onlist(1);
        this.get_departamento();
        

    },
    methods: {
        activate_cam: function () {
            $("#web-cam").attr("src", "web-cam/foto.html");
        },
        refresh:function(f=function(){}){
            var a =this;
            a.optionsSelect2 = a.optionsSelect2aux.filter(post => {
                return post.cod_depto.toLowerCase().includes($("#departamento_exp").val())
            });
            f();
        },
        get_departamento:function(){
            var a=this;
            $.get("Services/JSON/departamentos.json", function (data) {
                a.optionsSelect1=data;
                a.get_municipios();
            });
        },
        get_municipios:function(){
            var a=this;
            $.get("Services/JSON/municipios.json", function (data) {
                a.optionsSelect2aux=data;
                a.refresh();
                
            });
        },
        ondeleted: function (data) {
            var self = this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Alumnos/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Alumnos";
            $("#foto").val(data.foto);
            $("#imgfoto").attr("src",data.foto);
            $("#nombre").val(data.nombre);
            $("#apellidos").val(data.apellidos);
            $("#fecha_nacimiento").val(data.fecha_nacimiento);
            $("#identificacion").val(data.identificacion);
            $("#tipo_identificacion").val(data.tipo_identificacion);
            $("#sexo").val(data.sexo);
            $("#direccion").val(data.direccion);
			$("#passwordbox").hide();
            $("#password").val(data.password);
            $("#tipo").val(data.tipo);
            $("#telefono").val(data.telefono);
            $("#tipo_sangre").val(data.tipo_sangre);
            $("#sede").val(data.sede);
            $("#eps").val(data.eps);
            $("#prepagada").val(data.prepagada);
            $("#email").val(data.email);
            $("#zona").val(data.zona);
            $("#lugar_nac").val(data.lugar_nac);
            $("#departamento_exp").val(data.departamento_exp);
            this.refresh(function(){
                setTimeout(function(){
                    $("#ciudad_exp").val(data.ciudad_exp);
                },600);
                
            });
        },
        onlist: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Alumnos/list.php?paginas=15&page=' + page + '&nombre=' + $('#search').val() + '&apellidos=' + $('#search').val() + '&identificacion=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }

                $("#botonFoto").on({
                    click: function () {
                        window.frames['web-cam'].take_snapshot(function (url) {
                            $("#foto").val(url);
                            $("#imgfoto").attr("src", url);
                            $("#status_picture").css("color", "green");
                            $("#web-cam").attr("src", "#");
                        });
                    }
                });

                $("#logo2").change(function (evt) {
                    handleFileSelect(evt, "logo", function (img) {
                        $("#imgfoto").attr("src", "data:image/jpg;base64," + img);
                        $("#foto").val("data:image/jpg;base64," + img);

                    });
                });
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    data:  $("#form_Alumnos").serialize(),
                    type: "post",
                    url: "Services/Alumnos/save.php" ,
                    result: function (data) {
                        console.log(data);
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Alumnos se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        if (data.completed != "exist") {
                            self.id = 0;
                            $("#foto").val("");
                            $("#nombre").val("");
                            $("#apellidos").val("");
                            $("#fecha_nacimiento").val("");
                            $("#identificacion").val("");
                            $("#direccion").val("");
                            $("#password").val("");
                            $("#telefono").val("");
                            $("#eps").val("");
                            $("#email").val("");
                            $("#passwordbox").show();
                            $("#status_picture").css("color", "red");
                            $("#imgfoto").attr("src", "images/avatar.svg");
                            $("#zona").val("");
                            $("#lugar_nac").val("");
                            self.onlist(1);
                            self.operation = "Agregar Alumnos";
                        }else{
                            swal({
                                type: 'warning',
                                title: 'El Estudiante ya existe no hemos podido procesar la solicitud',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                    }
                });
            }
        }
    }
});
