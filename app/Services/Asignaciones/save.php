<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$asi=new Models\Asignaciones();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("POST");
	$asi->__set("grado_id",$grado_id);
	$asi->__set("nombre",$nombre);
	$asi->__set("tipo",$tipo);
	$asi->__set("horas",$horas);
	$asi->__set("horas_min",$horas_min);
	$asi->__set("grupo",$grupo);


	//si el proceso es update se miden dos factores si hay archivo de cambio o no//
	if(file_exists($_FILES['files']['tmp_name'][0])) {
		//eliminamos el archivo para aggregar el nuevo
		if(!$id==0){
			if($syllabus!=''){
				unlink('../../Syllabus/'.$syllabus);
			}
			
		}
		$nombre_archivo=$info->generate_keys().".pdf";
		$asi->__set("syllabus",$nombre_archivo);
		foreach($_FILES['files']['error'] as $key => $error){
			if($error == UPLOAD_ERR_OK){
				$name = $_FILES['files']['name'][$key];
				move_uploaded_file($_FILES['files']['tmp_name'][$key], '../../Syllabus/' . $name);
				rename("../../Syllabus/".$name, '../../Syllabus/'.$nombre_archivo);
				chmod('../../Syllabus/'.$nombre_archivo, 0777);
			}
		}
	}else{
		$asi->__set("syllabus","");
	}

    if($id==0) {
        $asi->__set("id", null);
        $result->completed = boolval($asi->save());
    }else{
        $asi->__set("id", $id);
        $result->completed = boolval($asi->update());
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>