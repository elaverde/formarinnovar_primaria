<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$usu=new Models\Admin();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$usu->__set("password",md5($password));
	$usu->__set("id", $id);
    $result->completed = boolval($usu->update_password());
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>