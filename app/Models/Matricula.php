<?php
namespace Models;
class Matricula
{
	private $id;
	private $matriculador;
	private $idalumno;
	private $idgrado;
	private $periodo;
	private $grado;
	private $fecha;
	//auxiliares//
	private $nombre;
	private $apellidos;
	private $identificacion;
	
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO matricula(id, matriculador, alumnos_id, grado_id, periodo, activo, fecha) VALUES (:id, :matriculador, :alumnos_id, :grado_id, :periodo, :activo, :fecha)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':matriculador'=>$this->__get('matriculador'),
			':alumnos_id'=>$this->__get('idalumno'),
			':grado_id'=>$this->__get('idgrado'),
			':periodo'=>date("Y"),
			':activo'=>1,
			':fecha'=>date("Y-m-d H:i:s"),
        ));
        $r["affected"]=$resultado;
        $r["id"]=$this->con->lastId("id");
        return $r;
    }
   
    public function update(){
        $sql="UPDATE grado SET nombre= :nombre WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM grado WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list_alumnos($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT
            Alu.id,
            Alu.nombre,
            Alu.apellidos,
            Alu.identificacion,
            Alu.foto
        FROM
            alumnos Alu
        LEFT JOIN
            matricula Mat
        ON
            Alu.id = Mat.alumnos_id
        WHERE
            (( Mat.periodo < :periodo AND (Select COUNT(*) from matricula where periodo=:periodo and matricula.alumnos_id = Mat.alumnos_id) = 0) OR 
            Mat.periodo IS NULL ) AND
            (Alu.nombre LIKE :nombre OR 
            Alu.apellidos LIKE :apellidos OR  
            Alu.identificacion LIKE :identificacion) 
        GROUP BY Alu.id
        LIMIT $start, $finish";

        $resultado1 = $this->con->ejecutarConsulta($sql,array(
            ':periodo'=>date("Y"),
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  
            COUNT(*) as results 
            FROM
            alumnos Alu
        LEFT JOIN
            matricula Mat
        ON
            Alu.id = Mat.alumnos_id
        WHERE
            (( Mat.periodo < :periodo AND (Select COUNT(*) from matricula where periodo=:periodo) = 0) OR 
            Mat.periodo IS NULL ) AND
            (Alu.nombre LIKE :nombre OR 
            Alu.apellidos LIKE :apellidos OR  
            Alu.identificacion LIKE :identificacion)";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
            ':periodo'=>date("Y"),
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;
        return $result;
    }

    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}