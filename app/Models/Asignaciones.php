<?php
namespace Models;
class Asignaciones
{
	private $id;
	private $grado_id;
	private $nombre;
	private $tipo;
	private $horas;
	private $horas_min;
	private $grupo;
	private $syllabus;
    private $con;
    public function __construct(){
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO asignaciones (id, grado_id, nombre, tipo, horas, horas_min, grupo, syllabus) VALUES (:id, :grado_id, :nombre, :tipo, :horas, :horas_min, :grupo, :syllabus)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':grado_id'=>$this->__get('grado_id'),
			':nombre'=>$this->__get('nombre'),
			':tipo'=>$this->__get('tipo'),
			':horas'=>$this->__get('horas'),
			':grupo'=>$this->__get('grupo'),
			':syllabus'=>$this->__get('syllabus'),
			':horas_min'=>$this->__get('horas_min')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE asignaciones SET grado_id= :grado_id, nombre= :nombre, tipo= :tipo, horas= :horas, horas_min= :horas_min, grupo = :grupo, syllabus = :syllabus WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':grado_id'=>$this->__get('grado_id'),
            ':nombre'=>$this->__get('nombre'),
            ':tipo'=>$this->__get('tipo'),
            ':horas'=>$this->__get('horas'),
            ':grupo'=>$this->__get('grupo'),
            ':syllabus'=>$this->__get('syllabus'),
			':horas_min'=>$this->__get('horas_min')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM asignaciones WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list_select_by_grado(){
         $result=  new  \stdClass();
        $sql="SELECT * FROM asignaciones WHERE grado_id = :grado_id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
            ':grado_id'=>$this->__get("grado_id")
        ));
        $result->data=$resultado1;
        return $result;
    }
    public function list_asignaciones_all_by_grado(){
        $sql="SELECT g.id as dni, g.nombre as grado, a.id, a.nombre, a.tipo, a.horas, a.horas_min  FROM asignaciones a, grado g WHERE g.id=a.grado_id ORDER BY g.id, a.nombre";
        $resultado1 = $this->con->ejecutarConsulta($sql);
        return $resultado1;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM asignaciones WHERE  nombre LIKE :nombre and grado_id = :grado_id LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
            ':nombre'=>'%'.$this->__get('nombre').'%',
            ':grado_id'=>$this->__get('grado_id')
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM asignaciones WHERE  nombre LIKE :nombre  and grado_id = :grado_id";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
            ':grado_id'=>$this->__get('grado_id')
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}