<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$seg=new Models\Seguimiento();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $seg->__set("matricula_id",$matricula_id);
    $data=$seg->total_seguimiento();
    $result->count = $data;
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>