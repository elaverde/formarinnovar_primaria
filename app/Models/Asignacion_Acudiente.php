<?php

namespace Models;
class Asignacion_Acudiente
{
	private $alumnos_id;
	private $acudiente_id;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO alumnos_has_acudiente (alumnos_id, acudiente_id) VALUES (:alumnos_id, :acudiente_id)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':alumnos_id'=>$this->__get('alumnos_id'),
			':acudiente_id'=>$this->__get('acudiente_id')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE alumnos_has_acudiente SET acudiente_id= :acudiente_id WHERE alumnos_id = :alumnos_id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':alumnos_id'=>$this->__get('alumnos_id'),
			':acudiente_id'=>$this->__get('acudiente_id')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM alumnos_has_acudiente WHERE alumnos_id = :alumnos_id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM alumnos_has_acudiente WHERE alumnos_id LIKE :alumnos_id OR  acudiente_id LIKE :acudiente_id LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':alumnos_id'=>'%'.$this->__get('alumnos_id').'%',
			':acudiente_id'=>'%'.$this->__get('acudiente_id').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM alumnos_has_acudiente WHERE alumnos_id LIKE :alumnos_id OR  acudiente_id LIKE :acudiente_id ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':alumnos_id'=>'%'.$this->__get('alumnos_id').'%',
			':acudiente_id'=>'%'.$this->__get('acudiente_id').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}