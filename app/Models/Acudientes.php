<?php

namespace Models;
class Acudientes
{
	private $id;
	private $foto;
	private $nombre;
	private $apellidos;
	private $fecha_nacimiento;
	private $identificacion;
	private $sexo;
	private $direccion;
	private $password;
	private $parentesco;
	private $telefono;
	private $email;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO acudiente (id, foto, nombre, apellidos, fecha_nacimiento, identificacion, sexo, direccion, password, parentesco, telefono, email) VALUES (:id, :foto, :nombre, :apellidos, :fecha_nacimiento, :identificacion, :sexo, :direccion, :password, :parentesco, :telefono, :email)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':password'=>$this->__get('password'),
			':parentesco'=>$this->__get('parentesco'),
			':telefono'=>$this->__get('telefono'),
			':email'=>$this->__get('email')
        ));
        $r["affected"]=$resultado;
        $r["id"]=$this->con->lastId("id");
        return $r;
    }

    public function get_alumnos(){
        $sql =  "SELECT
                    alumnos.*, matricula.id as matricula,
                    grado.id as gradoid,
                    grado.nombre as grado
                FROM
                    alumnos_has_acudiente
                INNER JOIN alumnos ON alumnos_has_acudiente.alumnos_id = alumnos.id
                INNER JOIN matricula ON matricula.alumnos_id = alumnos.id
                INNER JOIN grado ON matricula.grado_id = grado.id
                WHERE
                    alumnos_has_acudiente.acudiente_id = :id";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':id'=>$this->__get('id')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE acudiente SET foto= :foto, nombre= :nombre, apellidos= :apellidos, fecha_nacimiento= :fecha_nacimiento, identificacion= :identificacion, sexo= :sexo, direccion= :direccion,  parentesco= :parentesco, telefono= :telefono, email= :email WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':parentesco'=>$this->__get('parentesco'),
			':telefono'=>$this->__get('telefono'),
			':email'=>$this->__get('email')
        ));
        return $resultado;
    }
    public function update_password(){
        $sql="UPDATE acudiente SET password = :password WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function update_password_automatic(){
        $sql="UPDATE acudiente SET password = :password WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':identificacion'=>$this->__get('identificacion'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function get_user_by_dni(){
        $sql="SELECT email,nombre,apellidos,identificacion FROM acudiente WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':identificacion'=>$this->__get('identificacion')
        ));
        $user->email=$resultado[0]["email"];
        $user->nombre=$resultado[0]["nombre"];
        $user->apellidos=$resultado[0]["apellidos"];
        $user->identificacion=$resultado[0]["identificacion"];
        return $user;
    }
    public function delete(){
        $sql="DELETE FROM acudiente WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function exist(){
        $sql="SELECT * FROM acudiente WHERE identificacion=:identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':identificacion'=>$this->__get("identificacion")
        ));
        if(count($resultado)==0){
            return  false;
        }else{
            return  true;
        }
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM acudiente WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM acudiente WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function profile(){
        $sql="SELECT * FROM acudiente WHERE id=:id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':id'=>$this->__get('id')
        ));
        return $resultado1;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}