<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pro=new Models\Profesores();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$pro->__set("password",md5($password));
	$pro->__set("id", $id);
    $result->completed = boolval($pro->update_password());
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>