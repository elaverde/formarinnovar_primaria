<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$hor=new Models\Horarios();
$dis=new Models\Disponibilidad();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$hor->__set("profesores_id",$profesores_id);
	$hor->__set("asignacion",$asignacion_nombre);
	$hor->__set("periodo",date("Y"));
	$hor->__set("activo",1);
	$hor->__set("hora_inicio",$hora_inicio);
	$hor->__set("hora_final",$hora_final);
	$hor->__set("dia",$dia);
    if($id==0) {
		$hor->__set("id", null);
		$data=$hor->save();
		$result->completed = boolval($data["affected"]);
		foreach ($_GET["asignacion"] as $value){
			$dis->__set("id", null);
			$dis->__set("asignaciones_id",$value);
			$dis->__set("horarios_id",$data["id"]);
			$dis->save();
		}
    }else{
		$data=$hor->save();
        $result->completed = boolval($data["affected"]);
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>