elements = {
    
	"#grado_id": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Grado"
	},
	"#nombre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Nombre"
	}
}
var Grupos_Electivos = new Vue({
    el: '#Grupos_Electivos',
    data: {
        id: 0,
        operation: "Agregar Grupos_Electivos",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        optionsSelect1:[{text:"Cargando..",value:"0"}]
    },
    created: function () {
        this.onListGrados();
    },
    methods: {
        onListGrados:function(){
            var url = 'Services/Grados/select.php';
            var self=this;
            $.get(url, function (data) {
                datos=data.data;
                self.optionsSelect1=[];
                for (var i=0; i<datos.length; i++){
                    self.optionsSelect1.push({text:datos[i].nombre,value:datos[i].id});
                }
                setTimeout(function(){
                    self.onlist(1);
                },500);
            });
        },
        ondeleted: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Grupos_Electivos/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Grupos_Electivos";
            $("#grado_id").val(data.grado_id);
			$("#nombre").val(data.nombre);
			
        },
        onlist: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Grupos_Electivos/list.php?paginas=15&page=' + page + '&nombre=' + $('#search').val()+"&grado_id="+$("#grado_id").val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    url: "Services/Grupos_Electivos/save.php?" + $("#form_Grupos_Electivos").serialize(),
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Grupos_Electivos se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                    	$("#nombre").val("");
					    self.onlist(1);
                        self.operation = "Agregar Grupos_Electivos";
                    }
                });
            }
        }
    }
});