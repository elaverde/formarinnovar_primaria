<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";

Config\Autoload::run();
$db= new Models\Conexion();
$adm=new Models\Admin();
$info=new Models\Info_System();
$aut=new Models\Authentication();
$mail=new Messages();
$result=  new stdClass();
    $db->extract_dates("GET");
    $adm->__set("identificacion", $identificacion);
    $data=$adm->get_user_by_dni();
    if(empty($data->email)) {
        $result->completed=false;
        echo json_encode($result);
    }else{
        $password=$aut->generate_pass();
        $adm->__set("password",md5($password));
        $result->completed = boolval($adm->update_password_automatic());
        $mail->setPass($info->__get("pass"));
		$mail->setEmail($info->__get("email"));
		$mail->setNamefrom($info->__get("from"));
		$mail->setAsunto("Recuperación de contraseña");
        $mail->setMsg('<img width="700" src="https://formarinnovar.com/Services/images/logo_web-01.png"><h1>Estimado usuario,</h1><p style="font-size: 20px;">De acuerdo con su solicitud se ha generado una contraseña automática hecha por el sistema, recuerde que si desea cambiarla lo puede hacer por medio de modificaciones del módulo perfil</p><b>Usuario:</b> '.$identificacion.'<br><b>Password:</b> '.$password.' <br><br><a href="https://formarinnovar.com/Services/app/" target="_blank">Entrar</a>');
        $mail->setName($data->nombre." ".$data->apellidos);
		$mail->setPara($data->email);
        $mail->send($mail);
        echo json_encode($result);
    }
?>