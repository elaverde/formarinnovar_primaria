<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pro=new Models\Profesores();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("POST");
	$pro->__set("foto",$foto);
	$pro->__set("email",$email);
	$pro->__set("nombre",$nombre);
	$pro->__set("apellidos",$apellidos);
	$pro->__set("fecha_nacimiento",$fecha_nacimiento);
	$pro->__set("identificacion",$identificacion);
	$pro->__set("sexo",$sexo);
	$pro->__set("direccion",$direccion);
	$pro->__set("password",md5($password));
	$pro->__set("telefono",$telefono);
	$pro->__set("tipo_sangre",$tipo_sangre);
	if(!$pro->exist()){
		if($id==0) {
			$pro->__set("id", null);
			$result->completed = boolval($pro->save());
			$mail->setPass($info->__get("pass"));
			$mail->setEmail($info->__get("email"));
			$mail->setNamefrom($info->__get("from"));
			$mail->setAsunto("Bienvenido a la familia Formar Innovar");
			$mail->setMsg('<img width="700" src="https://formarinnovar.com/Services/images/logo_web-01.png"><h1>Estimado usuario,</h1><p style="font-size: 20px;">Nos complace darle la más cordial bienvenida al gran familia del Colegio campestre Formar Innovar.</p><i><b>“Un Lugar para ser Feliz”</b></i><p style="font-size: 16px;">Para el ingreso al sistema se generaron un usuario y contraseña</p><b>Usuario:</b> '.$identificacion.'<br><b>Password:</b> '.$password.' <br><br><a href="https://formarinnovar.com/Services/app/" target="_blank">Entrar</a>');
			$mail->setName($nombre." ".$apellidos);
			$mail->setPara($email);
			$mail->send($mail);
		}else{
			$pro->__set("id", $id);
			$result->completed = boolval($pro->update());
		}
		echo json_encode($result);
	}else{
		$result->completed = "exist";
		echo json_encode($result);
	}
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>