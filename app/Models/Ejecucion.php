<?php

namespace Models;
class Ejecucion
{
	private $id;
	private $matricula_id;
	private $id_plan;
	private $id_rubrica;
	private $promediorubrica;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO ejecucion_plan (id, matricula_id, id_plan, promediorubrica) VALUES (:id, :matricula_id, :id_plan, :promediorubrica)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':matricula_id'=>$this->__get('matricula_id'),
			':id_plan'=>$this->__get('id_plan'),
			':promediorubrica'=>NULL
        ));
        $r["affected"]=$resultado;
        $r["id"]=$this->con->lastId("id");
        return $r;
    }
    public function update(){
        $sql="UPDATE ejecucion_plan SET promediorubrica = :promediorubrica WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':promediorubrica'=>$this->__get('promediorubrica')
        ));
        return $resultado;
    }
    public function finish_plan(){
        $sql="SELECT COUNT(*) AS n, promediorubrica  FROM ejecucion_plan e WHERE e.id_plan = :id_plan  and e.promediorubrica <> '' and e.matricula_id = :matricula_id";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':id_plan'=>$this->__get('id_plan'),
			':matricula_id'=>$this->__get('matricula_id')
        ));
        $respuesta->score=$resultado[0]["promediorubrica"];
        if($resultado[0]["n"]>0){
            $respuesta->status=true;
        }else{
            $respuesta->status=false;
        }
        return $respuesta;
    }

    public function exists_ejecucion(){
        $sql="SELECT
            COUNT(*) AS Total
        FROM
        ejecucion_plan
        WHERE ejecucion_plan.matricula_id = :matricula_id AND
        ejecucion_plan.id_plan = :id_plan";

        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':id_plan'=>$this->__get('id_plan')
        ));
        if($resultado[0]["Total"]>0){
            return true;
        }else{
            return false;
        }
    }
    

    public function delete(){
        $sql="DELETE FROM ejecucion_plan WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM ejecucion_plan WHERE id LIKE :id LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':id'=>'%'.$this->__get('id').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM ejecucion_plan WHERE id LIKE :id ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':id'=>'%'.$this->__get('id').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}