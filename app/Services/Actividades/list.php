<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$act=new Models\Activity();
$info= new Models\Info_System();

$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$act->__set("plan_id",$plan_id);
    echo json_encode($act->get_activitys_by_plan());
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>