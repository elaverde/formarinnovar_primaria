<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$mat=new Models\Matricula();
$pen=new Models\Pensum();
$asi=new Models\Asignaciones();
$info=new Models\Info_System();
$result=new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$mat->__set("id", null);
    $mat->__set("matriculador", $info->get_session("fi_id"));
    $mat->__set("idalumno", $idalumno);
    $mat->__set("idgrado", $grado_id);
    $r=$mat->save();
    $result->completed = boolval($r["affected"]);
    $asi->__set("grado_id",$grado_id);
    $data=$asi->list_select_by_grado();
    $correcto=true;
    foreach($data->data as $val){
        
        $pen->__set("id",null);
        $pen->__set("matricula_id",$r["id"]);
        $pen->__set("id_asignacion",$val["id"]);
        $pen->__set("nombre_asignacion",$val["nombre"]);
        $pen->__set("tiempo_esperado",$val["horas"]);
        $pen->__set("tiempo",0);
        $pen->__set("grupo",$val["grupo"]);
        $pen->__set("completa",0);
        if(!$pen->save()){
            $correcto=false;
        }
    }
    if($result->completed and $correcto){
    }else{
        $result->completed=false;
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>