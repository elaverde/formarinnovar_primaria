<?php
/**
 * Created by PhpStorm.
 * User: elaverde
 * Date: 21/8/2018
 * Time: 22:55
 */
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();

$db= new Models\Conexion();
$recover=new Models\Authentication();
$info=new Models\Info_System();
$mail=new Messages();

$result=new stdClass();
$db->extract_dates("GET");
$recover->__set("email",$email);
$data=$recover->recovery();

if($data["recover"]==false){
    //el correo no existe
    $result->result=false;
}else{
    $result->result=true;

    $mail->setPass($info->__get("pass"));
    $mail->setEmail($info->__get("email"));
    $mail->setNamefrom($info->__get("from"));
    $mail->setAsunto("Recuperación de contraseña");
    $web ="Se asigno correctamente la nueva contraseña al Sistema de información Corpo Sumapaz <br>";
    $web.="Para acceder al Sistema utilice el siguiente enlace: <a href='http://www.corposumapaz.org/' target='_blank'>www.corposumapaz.org</a> <br>";
    $web.="Sus credenciales de acceso son:<br>";
    $web.="Usuario: ".$email."<br>";
    $web.="Contraseña: ".$data["pass"]."<br>";
    $mail->setMsg($web);
    $mail->setName($data["name"]." ".$data["last_name"]);
    $mail->setPara($data["email"]);
    $mail->send($mail);

}

echo json_encode($result);
?>