<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$act=new Models\Activity();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$act->__set("plan_id",$plan_id);
	$act->__set("actividad",$actividad);
	$act->__set("tiempo",$tiempo);

    if($id==0) {
        $act->__set("id", null);
        $result->completed = boolval($act->save());
    }else{
        $act->__set("id", $id);
        $result->completed = boolval($act->update());
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>