<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pro=new Models\Profesores();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$pro->__set("nombre",$nombre);
	$pro->__set("apellidos",$apellidos);
	$pro->__set("identificacion",$identificacion);
    echo json_encode($pro->list($page,$paginas));
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>