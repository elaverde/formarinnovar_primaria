$(function () {
  $('#btn-recovery').on({
    click: function () {
      $("#recover").show();
      $("#login").hide();
    }
  });
  $('#btn-login2').on({
    click: function () {
      $("#recover").hide();
      $("#login").show();
    }
  });

  $("#btn-recuperado").on({
    click: function () {
      $(this).hide();
      var url = '';
      switch ($("#tipo").val()) {
        case "1":
          // Estudiante
          url="Services/Alumnos/update_password_automatic.php?identificacion=";
          break;
        case "2":
          // Acudiente
          url="Services/Acudientes/update_password_automatic.php?identificacion=";
          break;
        case "3":
          // Profesor
          url="Services/Profesores/update_password_automatic.php?identificacion=";
          break;
        case "4":
          url="Services/Usuarios/update_password_automatic.php?identificacion=";
          // Administrativo
          break;
      }
      $.ajax({
        url: url + $('#dni').val(), 
        success: function (result) {
          $("#btn-recuperado").show();
          if (result.completed) {
            swal({
              type: 'success',
              text: 'Se ha enviado su nueva contraseña a su correo. Si no le aparece correo por favor revise spam de lo contrario póngase contacto con el administrador',
              showConfirmButton: false,
              timer: 9500,
            });
          } else {
            swal({
              type: 'error',
              title: 'upsss',
              html: 'Esta identificación con este rol al parecer no existe, por favor revise y si el problema persiste póngase en contacto con el administrador brindando sus datos.',
            });
          }
        },
        error: function (xhr, status, error) {
          swal({
            type: 'error',
            title: 'upsss',
            html: 'ha ocurrido un error inesperado al conectarse a internet intentelo mas tarde ' +
              error +
              ' ' +
              status +
              ' ' +
              xhr,
          });
        },
      });
    }
  });
  $('#btn-login').on({
    click: function () {
      if ($(this).val() == 'Inicia sesión') {
        $.ajax({
          url: 'Services/Login/access.php?dni=' +
            $('#email').val() +
            '&pass=' +
            $('#pass').val(),
          success: function (result) {
            console.log(result);
            if (result.access) {
              window.location.href = 'plataforma.php#';
            } else {
              swal({
                type: 'error',
                title: 'upsss',
                html: 'ha ocurrido un error revisa tus credenciales',
              });
            }
          },
          error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
            swal({
              type: 'error',
              title: 'upsss',
              html: 'ha ocurrido un error inesperado al conectarse a internet intentelo mas tarde ' +
                error +
                ' ' +
                status +
                ' ' +
                xhr,
            });
          },
        });
      } else {
        $.ajax({
          url: 'Services/Login/recovery.php?email=' + $('#email').val(),
          success: function (result) {
            if (result.result) {
              swal({
                type: 'success',
                title: 'Se ha enviado su nueva contraseña a su correo',
                showConfirmButton: false,
                timer: 3500,
              });
              $('#recovery').trigger('click');
            } else {
              swal({
                type: 'error',
                title: 'upsss',
                html: 'ha ocurrido un error revisa este correo ya que no existe',
              });
            }
          },
          error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
            swal({
              type: 'error',
              title: 'upsss',
              html: 'ha ocurrido un error inesperado al conectarse a internet intentelo mas tarde ' +
                error +
                ' ' +
                status +
                ' ' +
                xhr,
            });
          },
        });
      }
    },
  });

  $('#email,#pass').on('keydown', function(e) {
    if (e.which == 13) {
        $('#btn-login').trigger("click");
        e.preventDefault();
    }
  });

});