<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$alu=new Models\Alumnos();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("POST");
	$alu->__set("foto",$foto);
	$alu->__set("nombre",$nombre);
	$alu->__set("apellidos",$apellidos);
	$alu->__set("fecha_nacimiento",$fecha_nacimiento);
	$alu->__set("identificacion",$identificacion);
	$alu->__set("tipo_identificacion",$tipo_identificacion);
	$alu->__set("sexo",$sexo);
	$alu->__set("direccion",$direccion);
	$alu->__set("password",md5($password));
	$alu->__set("tipo",$tipo);
	$alu->__set("telefono",$telefono);
	$alu->__set("tipo_sangre",$tipo_sangre);
	$alu->__set("sede",$sede);
	$alu->__set("eps",$eps);
	$alu->__set("prepagada",$prepagada);
	$alu->__set("email",$email);
	$alu->__set('departamento_exp',$departamento_exp);
	$alu->__set('ciudad_exp',$ciudad_exp);
	$alu->__set('lugar_nac',$lugar_nac);
	$alu->__set('zona',$zona);
	
		if($id==0) {
			if(!$alu->exist()){
				$alu->__set("id", null);
				$result->completed = boolval($alu->save());
				$mail->setPass($info->__get("pass"));
				$mail->setEmail($info->__get("email"));
				$mail->setNamefrom($info->__get("from"));
				$mail->setAsunto("Bienvenido a la familia Formar Innovar");
				$mail->setMsg('<img width="700" src="https://formarinnovar.com/Services/images/logo_web-01.png"><h1>Estimado usuario,</h1><p style="font-size: 20px;">Nos complace darle la más cordial bienvenida al gran familia del Colegio campestre Formar Innovar.</p><i><b>“Un Lugar para ser Feliz”</b></i><p style="font-size: 16px;">Para el ingreso al sistema se generaron un usuario y contraseña</p><b>Usuario:</b> '.$identificacion.'<br><b>Password:</b> '.$password.' <br><br><a href="https://formarinnovar.com/Services/app/" target="_blank">Entrar</a>');
				$mail->setName($nombre." ".$apellidos);
				$mail->setPara($email);
				$mail->send($mail);
				echo json_encode($result);
			}else{
				$result->completed = "exist";
				echo json_encode($result);
			}  
		}else{
			$alu->__set("id", $id);
			$result->completed = boolval($alu->update());
			echo json_encode($result);
		}
		
	
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>