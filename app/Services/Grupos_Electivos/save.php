<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$gru=new Models\Grupos_Electivos();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
		$gru->__set("grado_id",$grado_id);
	$gru->__set("nombre",$nombre);

    if($id==0) {
        $gru->__set("id", null);
        $result->completed = boolval($gru->save());
    }else{
        $gru->__set("id", $id);
        $result->completed = boolval($gru->update());
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>