function Get_profesor(id) {
    return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();
        req.open('GET', 'Services/Profesores/get_profesor.php?id='+id);
        req.onload = function() {
          if (req.status == 200) {
            resolve(JSON.parse(req.response));
          }
          else {
           // reject();
          }
        };

        req.send();
    })
}
var Calificaciones = new Vue({
    el: '#Calificaciones',
    data: {
        id: 0,
        preload:false,
        competencia:["Cognitivo","Procedimental","Axiológico","Actitudinal","Comunicativo","Manejo de segunda lengua"],
        preload_list:false,
        file:null,
        optionsSelect1:[{text:"Cargando..",value:"0",archivo:""}],
        operation: "Calificaciones",
        list_archivos:null,
        preload_archivos:false,
        list3:null,
        alumnos:null,
        alumno:"",
        alumno_activo:[],
        foto:"",
        area:"",
        seguimiento:null,
        evaluacion:null,
        rowspan:0,
        placomplete:null
    },
    created: function () {
        s=this;
        s.get_alumnos();
    },
    methods: {
        get_competencia:function(id){
            return this.competencia[id];
        },
        get_dates_rubrica:function(id,proposito){
            var data=this.placomplete.rubricas;
            var n=data.length;
          //  this.rowspan=n;
            for(var i=0; i<n; i++){
                if(data[i].id==id){
                    return data[i][proposito];
                }
            }
        },
        get_dates_actividad:function(id,proposito){
            var data=this.placomplete.actividades;
            var n=data.length;
         //   this.rowspan=n;
            for(var i=0; i<n; i++){
                if(data[i].id==id){
                    return data[i][proposito];
                }
            }
        },
        get_profesor:function(data,id,cell){
            Get_profesor(data.profesor).then(r => {
                return r;
            }).then(r => {
                console.log(r);
                try {
                    $("#"+cell+id).html("<center><img class='img-circle zoom' style='width: 30px !important; height: 30px !important;' src='"+r[0].foto+"'><br> <b style='font-size:9px;'>"+r[0].nombre+" "+r[0].apellidos+"</b></center>");
                  }
                  catch(error) {
                }
            })
        },
        get_file:function(data){
            var url = 'Services/Actividades_pdf/list.php?plan_id='+data.id+"&nombre=";
            s.preload_archivos=true;
            $.get(url, function (data) {
                s.list_archivos=data;
                s.preload_archivos=false;
            });
        },
        get_plan:function(data){
            var url = 'Services/Actividades/generar_plan_seguimiento_rubrica.php?plan_id='+data.id+"&matricula_id="+s.alumno_activo.matricula;
            //window.open(url,"_blank");
            $.get(url, function (data) {
                s.placomplete=data;
                s.seguimiento=data.seguimiento;
                s.rowspan=s.placomplete.actividades.length;
                //alert(s.rowspan);
                s.evaluacion=data.evaluacion;
                s.area=$("#asignaciones_id option:selected").text();
                setTimeout(function(){
                    s.evaluar_rubrica();
                },1000);
                
            });
        },
        evaluar_rubrica:function(){
            var n=this.placomplete.evaluacion.length;
            var total=0;
            $("#ejecucion_plan_id").val(this.placomplete.evaluacion[0].ejecucion_plan_id);
            for(var i=0; i<n; i++){
                var value = isNaN(parseInt($("#c"+i+"s").val())) ? 0 : parseInt($("#c"+i+"s").val());
                total=total+(value*(parseInt($("#c"+i).text())/100));
            }
            $("#R_total").html("<center>"+total.toFixed(2)+"</center>");
            $("#notafinal").val(total.toFixed(2));
        },
        onloadPDF:function(){
            s.area=$("#asignaciones_id option:selected").text();
            s.file=$("#asignaciones_id option:selected").attr("data-file");
        },
        onListAsignaciones:function(){
            var url = 'Services/Asignaciones/list_select_by_grado.php?grado_id='+s.alumno_activo.gradoid;
            $.get(url, function (data) {
                datos=data.data;
                s.optionsSelect1=[];
                for (var i=0; i<datos.length; i++){
                    s.optionsSelect1.push({text:datos[i].nombre,value:datos[i].id,archivo:datos[i].syllabus});
                }
               
                setTimeout(function(){
                    s.onlistPlansRealizados(1);
                },400);
                
            });
        },
        onlistPlansRealizados: function (page) {
            s.preload=true;
            s.onloadPDF();
            var url = 'Services/Plan/list_realizados.php?paginas=150&page=' + page + '&asignaciones_id=' + $('#asignaciones_id').val()+"&matricula_id="+s.alumno_activo.matricula;
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    s.list3 = data["data"];
                    s.preload=false;
                    console.log(data["data"]);
                }
            });
        },
        set_alumno:function(data){
            s.alumno_activo={
                fullnombre:data.nombre+" "+data.apellidos,
                foto:data.foto,
                gradoid:data.gradoid,
                grado:data.grado,
                matricula:data.matricula
            };
          
            s.alumno=s.alumno_activo.fullnombre;
            s.foto=s.alumno_activo.foto;
            s.onListAsignaciones();
        },
        get_alumnos:function () {
            var url="Services/Acudientes/get_alumnos.php";
            $.get(url, function (data) {
                s.alumnos=data;
                s.set_alumno(data[0]);
            });
        }
    }
});