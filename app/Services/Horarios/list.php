<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$hor=new Models\Horarios();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $data=$hor->list();
    $result = array();
    foreach ($data as $r) {
        $result["dia".$r['dia']][] = $r;
    }
   echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>