<?php
namespace Models;
class Activity
{
	private $id;
	private $plan_id;
	private $actividad;
	private $tiempo;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO activity (id, plan_id, actividad, tiempo) VALUES (:id, :plan_id, :actividad, :tiempo)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':plan_id'=>$this->__get('plan_id'),
			':actividad'=>$this->__get('actividad'),
			':tiempo'=>$this->__get('tiempo')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE activity SET plan_id= :plan_id, actividad= :actividad, tiempo= :tiempo WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':plan_id'=>$this->__get('plan_id'),
			':actividad'=>$this->__get('actividad'),
			':tiempo'=>$this->__get('tiempo')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM activity WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function get_activitys_by_plan(){
        $sql="SELECT
                    activity.id,
                    activity.actividad,
                    activity.plan_id,
                    activity.tiempo,
                    plan.aprendizaje,
                    plan.asignaciones_id,
                    plan.derechos,
                    plan.n
                FROM
                    activity
                INNER JOIN plan ON activity.plan_id = plan.id
                WHERE
                    activity.plan_id = :plan_id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id')
        ));
        return $resultado1;
    }

    
    
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}