elements = {

    "#foto": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Foto"
    },
    "#nombre": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Nombres"
    },
    "#apellidos": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Apellidos"
    },
    "#fecha_nacimiento": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Fecha de nacimiento"
    },
    "#identificacion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Identificación"
    },
    "#tipo_identificacion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo de identificación"
    },
    "#sexo": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Sexo"
    },
    "#direccion": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Dirección"
    },
    "#password": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Password"
    },
    "#tipo": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo"
    },
    "#telefono": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Teléfono"
    },
    "#tipo_sangre": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Tipo de sangre"
    },
    "#sede": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Sede"
    },
    "#eps": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Eps"
    },
    "#prepagada": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Prepagada"
    },
    "#email": {
        "obligatorio": true,
        "msg": "Por favor ingrese el Correo eletrónico"
    }
}
var Alumnos = new Vue({
    el: '#Alumnos',
    data: {
        id: 0,
        operation: "Agregar Alumnos",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
    },
    created: function () {
        this.onlist();
        

    },
    methods: {
        activate_cam: function () {
            $("#web-cam").attr("src", "web-cam/foto.html");
        },
        onupdatepassword:function(){
            if($("#password1").val()!=""){
                if($("#password2").val()==$("#password1").val()){
                    save({
                        type: "GET",
                        url: "Services/Alumnos/update_password.php?password="+$("#password1").val()+"&id="+$("#fi_id").val() ,
                        result: function (data) {
                            if (data.completed) {
                                swal({
                                    type: 'success',
                                    title: 'Se ha almacenado correctamente',
                                    showConfirmButton: false,
                                    timer: 3500
                                });
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }else{
                    swal({
                        type: 'question',
                        title: 'Revisa tu contraseñas no coinciden con la confirmación',
                        showConfirmButton: false,
                        timer: 3500
                    });
                }
            }else{
                swal({
                    type: 'error',
                    title: 'Ingresa una contraseña',
                    showConfirmButton: false,
                    timer: 3500
                });
            }
        },
        ondeleted: function (data) {
            var self = this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Alumnos/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Alumnos";
            $("#foto").val(data.foto);
            $("#imgfoto").attr("src",data.foto);
            $("#nombre").val(data.nombre);
            $("#apellidos").val(data.apellidos);
            $("#fecha_nacimiento").val(data.fecha_nacimiento);
            $("#identificacion").val(data.identificacion);
            $("#tipo_identificacion").val(data.tipo_identificacion);
            $("#sexo").val(data.sexo);
            $("#direccion").val(data.direccion);
			$("#passwordbox").hide();
            $("#password").val(data.password);
            $("#tipo").val(data.tipo);
            $("#telefono").val(data.telefono);
            $("#tipo_sangre").val(data.tipo_sangre);
            $("#sede").val(data.sede);
            $("#eps").val(data.eps);
            $("#prepagada").val(data.prepagada);
            $("#email").val(data.email);

        },
        onlist: function () {
            var self = this;
            var url = 'Services/Alumnos/get_profile.php?id='+$("#fi_id").val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.onupdate(data[0]);
                }
                $("#botonFoto").on({
                    click: function () {
                        window.frames['web-cam'].take_snapshot(function (url) {
                            $("#foto").val(url);
                            $("#imgfoto").attr("src", url);
                            $("#status_picture").css("color", "green");
                            $("#web-cam").attr("src", "#");
                        });
                    }
                });
                $("#logo2").change(function (evt) {
                    handleFileSelect(evt, "logo", function (img) {
                        $("#imgfoto").attr("src", "data:image/jpg;base64," + img);
                        $("#foto").val("data:image/jpg;base64," + img);
                    });
                });
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    data:  $("#form_Alumnos").serialize(),
                    type: "post",
                    url: "Services/Alumnos/save.php" ,
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Alumnos se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                        $("#foto").val("");
                        $("#nombre").val("");
                        $("#apellidos").val("");
                        $("#fecha_nacimiento").val("");
                        $("#identificacion").val("");
                        $("#direccion").val("");
                        $("#password").val("");
                        $("#telefono").val("");
                        $("#eps").val("");
                        $("#email").val("");
						$("#passwordbox").show();
						$("#status_picture").css("color", "red");
						$("#imgfoto").attr("src", "images/avatar.svg");
                        self.onlist(1);
                        self.operation = "Agregar Alumnos";
                    }
                });
            }
        }
    }
});
