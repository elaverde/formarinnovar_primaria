<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$rub=new Models\Rubrica();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("GET");
	$mejorar=null;
	$basico=null;
	$alto=null;
	$superior=null;
	$rub->__set("plan_id",$plan_id);
	$rub->__set("competencia",$competencia);
	$rub->__set("criterio_evaluacion",$criterio_evaluacion);
	$rub->__set("peso",$peso);
	$rub->__set("mejorar",$mejorar);
	$rub->__set("basico",$basico);
	$rub->__set("alto",$alto);
	$rub->__set("superior",$superior);
	$rub->__set("profesor",$_SESSION['fi_id']);
	$rub->__set("dia",date("Y-m-d"));

    if($id==0) {
        $rub->__set("id", null);
        $result->completed = boolval($rub->save());
    }else{
        $rub->__set("id", $id);
        $result->completed = boolval($rub->update());
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>