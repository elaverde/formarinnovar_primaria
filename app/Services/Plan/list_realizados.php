<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pla=new Models\Plan();
$eje=new Models\Ejecucion();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    //hacer llegar la matricula//
	$pla->__set("asignaciones_id",$asignaciones_id);
	$pla->__set("derechos","");
    $pla->__set("aprendizaje","");
    $data=$pla->list($page,$paginas);
    $datos= array();
    foreach ( $data->data as $value ) { 
        $eje->__set("id_plan",$value["id"]);
        $eje->__set("matricula_id",$matricula_id);
        $r=$eje->finish_plan();
        $datos[]= array(
            "id" => $value["id"],
            "ejecutado"=>$r->status,
            "score"=>$r->score,
            "asignaciones_id" => $value["asignaciones_id"],
            "derechos" => $value["derechos"],
            "aprendizaje" => $value["aprendizaje"],
            "n" => $value["n"],
            "grado_id" => $value["grado_id"]
        );

    }

    $f=array(
        "data"=>$datos
    );

    echo json_encode($f);


}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>