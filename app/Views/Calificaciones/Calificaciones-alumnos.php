<?php session_start(); ?>
<div id="Calificaciones" class="card card-outline-info">
    <div class="card-header">
        <h4 style="float:left;" class="m-b-0 text-white">{{operation}}</h4>
        <h6 style="float:right;" class="m-b-0 text-white">Grado: {{alumno_activo.grado}}</h6>
    </div>
    <div class="card-body">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="modal-body">
                    <div class="col-md-5">
                        <i class="fa fa-file" aria-hidden="true"></i>
                        <a v-bind:href="'Syllabus/'+file" target="_blank">Descargar Syllabus {{area}}</a>
                    </div>
                    <div class="col-md-12">
                        <select @change="onlistPlansRealizados(1)" id="asignaciones_id" name="asignaciones_id"
                            type="text" class="form-control form-control-line">
                            <option v-for="option in optionsSelect1" v-bind:data-file="option.archivo"
                                v-bind:value="option.value">
                                {{option.text}}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </nav>
        <input type="hidden" id="fullnombre" value="<?=$_SESSION['fi_name']." ".$_SESSION['fi_lastname'] ?>">
        <input type="hidden" id="gradoid" value="<?= $_SESSION['fi_grado_id'] ?>">
        <input type="hidden" id="grado" value="<?= $_SESSION['fi_grado_nombre']?>">
        <input type="hidden" id="matricula" value="<?= $_SESSION['fi_id_matricula']?>">
        <div class="card-body">
            <h3>Progreso</h3>
            <center>
                <div v-if="preload"> <img src="images/preload.svg" width="100"></div>
            </center>
            <div v-if="!preload" class="container">
                <div class="row">
                    <div v-for="i in list3" class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <div class="card">
                            <center>
                                <div style="margin-top:10px;" class="card-img-top">
                                    <span
                                        v-bind:class="{ 'round bg-success': i.ejecutado, 'round bg-danger':  !i.ejecutado }">
                                        <i class="fas fa-trophy"></i>
                                    </span>
                                </div>
                            </center>
                            <div class="card-body">
                                <h5 v-if="i.ejecutado">Promedio
                                    <span class="label label-info">{{ i.score }}</span>
                                </h5>
                                <p style="font-size:12px;" class="card-text">{{ i.aprendizaje }}</p>
                                <span style="font-size:14px;text-align: justify; font-weight: bold;" class="time">Semana
                                    {{ i.n }}</span><br>
                                <center>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning">Ver detalles</button>
                                        <button type="button"
                                            class="btn btn-warning dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" x-placement="bottom-start"
                                            style="position: absolute; transform: translate3d(69px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <!--<a @click="get_plan(i)" data-toggle="modal" data-target="#actividades"
                                                class="dropdown-item" href="#">Activiaddes</a> Suspendido a solicitud del doctor cometa-->
                                            <a @click="get_plan(i)" data-toggle="modal" data-target="#diseno"
                                                class="dropdown-item" href="#">Calificaciónes</a>
                                            <a @click="get_file(i)" data-toggle="modal" data-target="#descargables"
                                                class="dropdown-item" href="#">Actividades descargables</a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="actividades" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4><i class="fas fa-tasks text-inverse m-r-10"></i>Actividades a desarrollar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form target="_blank" method="get" id="form_Actividades" name="form_Actividades">
                        <table class="fi_table full-color-table full-info-table hover-table">
                            <thead>
                                <tr>
                                    <th colspan="5" scope="col">PLAN DE EJECUCIÓN DE ÁREA </th>
                                </tr>
                                <tr>
                                    <th colspan="5" scope="col"><b>Area:</b> {{area}} <b>Grado:</b>
                                        {{alumno_activo.grado}}
                                    </th>
                                </tr>
                                <tr>
                                    <th class="fi_th">Nª</th>
                                    <th class="fi_th" width="30%">APRENDIZAJE/ LEARNING</th>
                                    <th class="fi_th">ACTIVIDADES A DESARROLLAR/ACTIVITIES TO BE DEVELOPED</th>
                                    <th class="fi_th">EVALUADO</th>
                                    <th class="fi_th">PROFESOR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(i,contador) in seguimiento" :key="contador">
                                    <td v-bind:rowspan="rowspan" v-if="contador==0">
                                        {{get_dates_actividad(i.id_activity,"n")}}</td>
                                    <td v-bind:rowspan="rowspan" v-if="contador==0">
                                        {{get_dates_actividad(i.id_activity,"aprendizaje")}}</td>
                                    <td>{{get_dates_actividad(i.id_activity,"actividad")}}</td>
                                    <td>{{i.desempeno}}</td>
                                    <td v-bind:id="'cell'+contador">{{get_profesor(i,contador,'cell')}} </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="descargables" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4><i class="fas fa-tasks text-inverse m-r-10"></i>Actividades Descargables</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div
                        style=" width:100%; display: flex !important; align-items: center !important; justify-content: center !important;">
                        <img v-if="preload_archivos" width="100" height="100" src="images/preload.svg">
                    </div>
                    <table v-if="!preload_archivos" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Actividades</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in list_archivos">
                                <td style="font-size:12px;">{{ i.nombre }} <br>
                                    <a style="padding:3px; radius:10px;" class="box bg-light-success"
                                        v-bind:href="'Actividades/'+i.url" target="_blank">
                                        <i class="ti-arrow-circle-down"></i> Ver actividad
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="diseno" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Rúbrica
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form id="form_Rubrica" name="form_Rubrica" action="#">
                            <input type="hidden" id="notafinal" name="notafinal">
                            <input type="hidden" id="ejecucion_plan_id" name="ejecucion_plan_id">
                            <!--<p>{{status_rubrica}}</p>-->
                            <table v-if="!preload_list" class="fi_table full-color-table full-info-table hover-table">
                                <thead>
                                    <tr>
                                        <th style="font-size:14px;">Competencia</th>
                                        <th style="font-size:14px;">Criterios de Evaluación</th>
                                        <th style="font-size:14px;">Peso Evaluativo (%)</th>
                                       <!--<th style="font-size:14px;">Por Mejorar</th>
                                        <th style="font-size:14px;">Básico</th>
                                        <th style="font-size:14px;">Alto</th>
                                        <th style="font-size:14px;">Superior</th>-->
                                        <th style="font-size:14px;" class="text-nowrap">Juicio evaluativo</th>
                                        <th style="font-size:14px;" class="text-nowrap">Observación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(i,n) in evaluacion">
                                        <td style="font-size:12px;font-weight: bold;">
                                            {{ get_competencia(get_dates_rubrica(i.id_rubrica,"competencia")) }}</td>
                                        <td style="font-size:12px;">
                                            {{ get_dates_rubrica(i.id_rubrica,"criterio_evaluacion")}}</td>
                                        <td style="font-size:12px;" v-bind:id="'c'+n">
                                            {{ get_dates_rubrica(i.id_rubrica,"peso") }}</td>
                                        <!--<td style="font-size:12px;">{{ get_dates_rubrica(i.id_rubrica,"mejorar") }}</td>
                                        <td style="font-size:12px;">{{ get_dates_rubrica(i.id_rubrica,"basico")  }}</td>
                                        <td style="font-size:12px;">{{ get_dates_rubrica(i.id_rubrica,"alto")  }}</td>
                                        <td style="font-size:12px;">{{ get_dates_rubrica(i.id_rubrica,"superior")  }}
                                        </td>-->
                                        <td style="font-size:12px;font-weight: bold;">
                                            <center>{{i.nota}}</center> <input type="hidden" v-bind:id="'c'+n+'s'"
                                                v-bind:value="i.nota" name="notas[]"
                                                class="form-control form-control-line">
                                        </td>
                                        <td style="font-size:12px; font-style: italic;">"{{i.comentario}}" <br><span
                                                v-bind:id="'cell2'+n"> {{get_profesor(i,n,'cell2')}}</span> </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; text-aling:center;" colspan="7">Total</td>
                                        <td style="font-weight: bold;" id="R_total">0</td>
                                        <td id="R_total"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="Views/Calificaciones/Calificaciones-alumnos.js?v=0.02"></script>