<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$gra=new Models\Grados();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $gra->__set("id",$id);
    $result->completed=boolval($gra->delete());
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>