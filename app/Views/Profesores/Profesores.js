elements = {
    
	"#foto": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Foto"
	},
	"#nombre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Nombre"
	},
	"#apellidos": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Apellidos"
	},
	"#fecha_nacimiento": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Fecha de nacimiento"
	},
	"#identificacion": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Identificación"
	},
	"#sexo": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Sexo"
	},
	"#direccion": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Dirección"
	},
	"#password": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Password"
	},
	"#telefono": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Teléfono"
	},
	"#tipo_sangre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Tipo de sangre"
	}
}
var Profesores = new Vue({
    el: '#Profesores',
    data: {
        id: 0,
        operation: "Agregar Profesores",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
    },
    created: function () {
        this.onlist(1);
    },
    methods: {
        activate_cam: function () {
            $("#web-cam").attr("src", "web-cam/foto.html");
        },
        ondeleted: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Profesores/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Profesores";
            $("#foto").val(data.foto);
            $("#imgfoto").attr("src",data.foto);
			$("#nombre").val(data.nombre);
			$("#apellidos").val(data.apellidos);
			$("#fecha_nacimiento").val(data.fecha_nacimiento);
			$("#identificacion").val(data.identificacion);
			$("#sexo").val(data.sexo);
			$("#direccion").val(data.direccion);
			$("#password").val(data.password);
			$("#telefono").val(data.telefono);
            $("#tipo_sangre").val(data.tipo_sangre);
            $("#passwordbox").hide();
            $("#email").val(data.email);
        },
        onlist: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Profesores/list.php?paginas=15&page=' + page + '&nombre=' + $('#search').val() + '&apellidos=' + $('#search').val() + '&identificacion=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }

                $("#botonFoto").on({
                    click: function () {
                        window.frames['web-cam'].take_snapshot(function (url) {
                            $("#foto").val(url);
                            $("#imgfoto").attr("src", url);
                            $("#status_picture").css("color", "green");
                            $("#web-cam").attr("src", "#");
                        });
                    }
                });

                $("#logo2").change(function (evt) {
                    handleFileSelect(evt, "logo", function (img) {
                        $("#imgfoto").attr("src", "data:image/jpg;base64," + img);
                        $("#foto").val("data:image/jpg;base64," + img);

                    });
                });
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    data: $("#form_Profesores").serialize(),
                    type: "post",
                    url: "Services/Profesores/save.php" ,
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Profesores se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        if (data.completed != "exist") {
                            self.id = 0;
                            $("#foto").val("");
                            $("#nombre").val("");
                            $("#apellidos").val("");
                            $("#fecha_nacimiento").val("");
                            $("#identificacion").val("");
                            $("#direccion").val("");
                            $("#password").val("");
                            $("#email").val("");
                            $("#telefono").val("");
                            $("#passwordbox").show();
                            $("#status_picture").css("color", "red");
                            $("#imgfoto").attr("src", "images/avatar.svg");
                            self.onlist(1);
                            self.operation = "Agregar Profesores";
                        }else{
                            swal({
                                type: 'warning',
                                title: 'El profesor ya existe no hemos podido procesar la solicitud',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                    }
                });
            }
        }
    }
});