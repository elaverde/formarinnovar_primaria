<?php
namespace Models;
class Horarios
{
	private $id;
	private $profesores_id;
	private $asignacion;
	private $periodo;
	private $activo;
	private $hora_inicio;
	private $hora_final;
	private $dia;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO horarios (id, profesores_id, asignacion, periodo, activo, hora_inicio, hora_final, dia) VALUES (:id, :profesores_id, :asignacion, :periodo, :activo, :hora_inicio, :hora_final, :dia)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':profesores_id'=>$this->__get('profesores_id'),
			':asignacion'=>$this->__get('asignacion'),
			':periodo'=>$this->__get('periodo'),
			':activo'=>$this->__get('activo'),
			':hora_inicio'=>$this->__get('hora_inicio'),
			':hora_final'=>$this->__get('hora_final'),
			':dia'=>$this->__get('dia')
        ));
        $r["affected"]=$resultado;
        $r["id"]=$this->con->lastId("id");
        return $r;
    }
    public function update(){
        $sql="UPDATE horarios SET profesores_id= :profesores_id, asignacion= :asignacion, periodo= :periodo, activo= :activo, hora_inicio= :hora_inicio, hora_final= :hora_final, dia= :dia WHERE id = :id";
        $resultado= $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':profesores_id'=>$this->__get('profesores_id'),
			':asignacion'=>$this->__get('asignacion'),
			':periodo'=>$this->__get('periodo'),
			':activo'=>$this->__get('activo'),
			':hora_inicio'=>$this->__get('hora_inicio'),
			':hora_final'=>$this->__get('hora_final'),
			':dia'=>$this->__get('dia')
        ));
        $r["affected"]=$resultado;
        $r["id"]=$this->con->lastId("id");
        return $r;
    }
    public function delete(){
        $sql="DELETE FROM horarios WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list_my_matriculas(){
        $sql="SELECT
            matricula_has_horarios.id,
            matricula_has_horarios.matricula_id,
            matricula_has_horarios.horarios_id
        FROM
            matricula_has_horarios
        INNER JOIN matricula ON matricula_has_horarios.matricula_id = matricula.id
        WHERE
            matricula_has_horarios.matricula_id = :id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
            ':id'=>$this->__get("id")
        ));
        //buscamos los n resultados
        return $resultado1;
    }
    public function list_my_horario($data){
        $add='';
        foreach ($data as $valor) {
            if($valor===end($data)){
                $add = $add." disponibles.asignaciones_id = ".$valor;
            }else{
                $add = $add." disponibles.asignaciones_id = ".$valor." or";
            }
        }
        $sql="SELECT
            profesores.foto,
            profesores.nombre,
            profesores.apellidos,
            profesores.telefono,
            profesores.email,
            horarios.id,
            horarios.profesores_id,
            horarios.activo,
            horarios.asignacion,
            horarios.dia,
            horarios.hora_final,
            horarios.hora_inicio,
            horarios.periodo,
            disponibles.asignaciones_id
        FROM
            disponibles
        INNER JOIN horarios ON disponibles.horarios_id = horarios.id
        INNER JOIN profesores ON horarios.profesores_id = profesores.id
        WHERE".$add;
        $resultado1 = $this->con->ejecutarConsulta($sql);
        //buscamos los n resultados
        return $resultado1;
    }
    public function list(){
        $sql="SELECT
                p.foto,
                p.nombre,
                p.apellidos,
                p.telefono,
                p.email,
                h.id,
                h.profesores_id,
                h.activo,
                h.asignacion,
                h.dia,
                h.hora_final,
                h.hora_inicio,
                h.periodo
            FROM
            horarios AS h
            INNER JOIN profesores AS p ON h.profesores_id = p.id
            WHERE h.activo=1 ORDER BY  h.dia, h.hora_inicio ";
        $resultado1 = $this->con->ejecutarConsulta($sql);
        //buscamos los n resultados
        return $resultado1;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}