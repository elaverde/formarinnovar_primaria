<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db = new Models\Conexion();
$eje = new Models\Ejecucion();
$act = new Models\Activity();
$seg= new Models\Seguimiento();
$rub = new Models\Rubrica();
$eva = new Models\Evaluacion();
$info = new Models\Info_System();
$result = new stdClass();
if ($info->session_active()) {
    $db->extract_dates("GET");
    $finish=true;
    //Primer paso generamos plan para generar el valor foraneo de seguimiento y rubricas//
    $eje->__set("id", null);
    $eje->__set("matricula_id", $matricula_id);
    $eje->__set("id_plan", $plan_id);
   // if (!$eje->exists_ejecucion()) { se suspendio si agregaban nuevas no actualizaba deprecated//
        $r = $eje->save();
        if ($r["affected"]) {
           
            //para generar el plan de seguimiento se crean rubricas y seguimientos//
            //1. Genaracion de segumientos
            $act->__set("plan_id", $plan_id);
            $actividades = $act->get_activitys_by_plan();
            foreach ($actividades as $value) {
                //seteamos valores de consulta para saber si no existe seguimiento//
                $seg->__set("plan_id", $plan_id);
                $seg->__set("id_activity", $value["id"]);
                //hacemos query consulta si seguimiento no existe se inserta para todas las actividades propuestas//
                if (!$seg->exists_seguimiento()) {
                    $seg->__set("id", null);
                    $seg->__set("ejecucion_plan_id", $r["id"]);
                    $seg->__set("id_activity", $value["id"]);
                    $seg->__set("dia", date("Y-m-d H:i:s"));
                    $seg->__set("profesor", $_SESSION['fi_id']);
                    $seg->__set("tema", $value["actividad"]);
                    $seg->__set("desempeno", "");
                    $res1=$seg->save();
                    if(!$res1){
                        $finish=false;
                    }
                }
            }
            //2. Generacion de rubricas//
            $rub->__set("plan_id", $plan_id);
            $rubrica = $rub->get_rubrica_by_plan();
            foreach ($rubrica as $value) {
              
                //seteamos valores de consulta para saber si no existe rubrica//
                $eva->__set("id_plan", $plan_id);
              //  $eva->__set("id_rubrica", $value["id"]);
                $eva->__set("matricula_id", $matricula_id);
                $eva->__set("competencia", $value["competencia"]);
        
                if (!$eva->exists_ejecucion()) {
                    //echo "wiii";
                    $eva->__set("id", null);
                    $eva->__set("ejecucion_plan_id", $r["id"]);
                    $eva->__set("id_rubrica", $value["id"]);
                    $eva->__set("competencia", $value["competencia"]);
                    $eva->__set("nota", "");
                    $eva->__set("comentario", "");
                    $eva->__set("dia", date("Y-m-d H:i:s"));
                    $eva->__set("profesor", $_SESSION['fi_id']);
                    $res2=$eva->save();
                    if(!$res2){
                        $finish=false;
                    }
                }
            }
            list_all($seg,$eva,$act,$rub,$matricula_id,$plan_id);
        }
    //}else{
    //    list_all($seg,$eva,$act,$rub,$matricula_id,$plan_id);
    //}
} else {
    $result->completed = "expired";
    echo json_encode($result);
}
function list_all($seg,$eva,$act,$rub,$matricula_id,$plan_id){
    $seg->__set("matricula_id", $matricula_id);
    $seg->__set("id_plan", $plan_id);
    $eva->__set("matricula_id", $matricula_id);
    
    $eva->__set("id_plan", $plan_id);
    $act->__set("plan_id", $plan_id);
    $rub->__set("plan_id", $plan_id);
    $data = array(
        "complete"=>true,
        "seguimiento" => $seg->list(),
        "evaluacion" => $eva->list(),
        "actividades"=> $act->get_activitys_by_plan(),
        "rubricas"=>$rub->get_rubrica_by_plan()
    );
    echo json_encode($data);
}
?>