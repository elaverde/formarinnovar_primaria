<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$pla=new Models\Plan();
$info= new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$pla->__set("asignaciones_id",$asignaciones_id);
	$pla->__set("derechos",$derechos);
	$pla->__set("aprendizaje",$aprendizaje);
    echo json_encode($pla->list($page,$paginas));
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>