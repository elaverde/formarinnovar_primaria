/*
Template Name: Atlas
Author: Edilson Laverde Molina
Email: edilsonlaverde_182@hotmail.com
File: js
Versión: 0.01
*/
var Atlas=function(id){
    this.id=id;
    return this;
}
var Upload_file=function(u){
    var check_ext=function(ext,options){
        for(var i=0; i<options.length; i++){
            if(ext==options[i]){
                return true;
            }
        }
        return false;
    }
    this.formdata=false;
    //Revisamos si el navegador soporta el objeto FormData
    if (window.FormData) {
        this.formdata = new FormData();
        var ext=document.getElementById(this.id).value.split('.');
        //Tomamos el ultimo trozo y lo convertimos a minuscula//
        ext=ext[ext.length-1].toLowerCase();
        if (document.getElementById(this.id).value != '' || (document.getElementById(this.id).value != '' == u.rules.required_file)) {
            if(check_ext(ext, u.rules.ext) || (u.rules.required_file==false && document.getElementById(this.id).value == '') ){
                u.events.On_Starting();
                var i = 0, len = document.getElementById(this.id).files.length, img, reader, file;
                //Si hay varias imágenes, las obtenemos una a una
                for (; i < len; i++) {
                    file = document.getElementById(this.id).files[i];
                    if (window.FileReader) {
                        reader = new FileReader();
                        //Llamamos a este evento cuando la lectura del archivo es completa
                        //Después agregamos la imagen en una lista
                        reader.onloadend = function (e) {
                            //Plan.onlist_archivos(1);
                        };
                        //Comienza a leer el archivo
                        //Cuando termina el evento onloadend es llamado
                        reader.readAsDataURL(file);
                    }
                    //Si existe una instancia de FormData
                    if (this.formdata){
                        //Usamos el método append, cuyos parámetros son:
                        formdata.append('files[]', file);
                        if(file.size>u.rules.size){
                            this.formdata=false;
                        }
                    }
                }
                if (this.formdata) {
                    for(var i=0; i<u.data.length; i++){
                        this.formdata.append(u.data[i].key, u.data[i].value);
                    }
                    $.ajax({
                        url: u.url,
                        type: 'POST',
                        data: this.formdata,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            u.events.On_Success(res);
                        }
                    });
                }else{
                    u.events.On_Exceeded_Size();
                }
            }else{
                u.events.On_Fail_Extension();
            }
        }else{
            u.events.On_Fail_File();
        }
    }
}