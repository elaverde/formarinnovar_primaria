<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$adm=new Models\Admin();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("POST");
	$adm->__set("foto",$foto);
	$adm->__set("nombre",$nombre);
	$adm->__set("apellidos",$apellidos);
	$adm->__set("fecha_nacimiento",$fecha_nacimiento);
	$adm->__set("identificacion",$identificacion);
	$adm->__set("sexo",$sexo);
	$adm->__set("direccion",$direccion);
	$adm->__set("password",md5($password));
	$adm->__set("tipo_rol",$tipo_rol);
	$adm->__set("telefono",$telefono);
	$adm->__set("tipo_sangre",$tipo_sangre);
	$adm->__set("email",$email);
		if($id==0) {
			if(!$adm->exist()){
				$adm->__set("id", null);
				$result->completed = boolval($adm->save());
				$mail->setPass($info->__get("pass"));
				$mail->setEmail($info->__get("email"));
				$mail->setNamefrom($info->__get("from"));
				$mail->setAsunto("Bienvenido a la familia Formar Innovar");
				$mail->setMsg('<img width="700" src="https://formarinnovar.com/Services/images/logo_web-01.png"><h1>Estimado usuario,</h1><p style="font-size: 20px;">Nos complace darle la más cordial bienvenida al gran familia del Colegio campestre Formar Innovar.</p><i><b>“Un Lugar para ser Feliz”</b></i><p style="font-size: 16px;">Para el ingreso al sistema se generaron un usuario y contraseña</p><b>Usuario:</b> '.$identificacion.'<br><b>Password:</b> '.$password.' <br><br><a href="https://formarinnovar.com/Services/app/" target="_blank">Entrar</a>');
				$mail->setName($nombre." ".$apellidos);
				$mail->setPara($email);
				$mail->send($mail);
				echo json_encode($result);
			}else{
				$result->completed = "exist";
				echo json_encode($result);
			}
		}else{
			$adm->__set("id", $id);
			$result->completed = boolval($adm->update());
			echo json_encode($result);
		}
	
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>