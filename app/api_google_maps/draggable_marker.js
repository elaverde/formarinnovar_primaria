/*
 * @fileoverview Sets up map with a single marker which can be
 * dragged around the map and report it's lat, lng.
 */
get_geomap = function () {
    return document.getElementById('marker-position').innerHTML;
}
coords = {
    lng: 0,
    lat: 0
};
initMap = function () {
    //usamos la API para geolocalizar el usuario
    navigator.geolocation.getCurrentPosition(
        function (position) {
            coords = {
                lng: position.coords.longitude,
                lat: position.coords.latitude
            };
            initialize();  //pasamos las coordenadas al metodo para crear el mapa

            return true;
        }, function (error) { console.log(error); });
    return true;
}
/*
 * Changes 'marker-state' div to show text.
 * @param: str String to write.
 */
function updateMarkerStateTxt(str) {
    document.getElementById('marker-state').innerHTML = str;
}
/*
 * Update the position of our 'marker-position' marker.
 * @param: latLng Maps LatLng object.
 */
function updateMarkerPositionTxt(latLng) {
    document.getElementById('marker-position').innerHTML =
        String(latLng.lat()) + ',' + String(latLng.lng());
}
/*
 * Sets up Google map into 'map-canvas' div and adds the
 * the starting marker and a drag listener.
 */
function initialize() {
    var latLng = new google.maps.LatLng(coords.lat, coords.lng);
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
        position: latLng,
        title: 'Estado del marcador',
        map: map,
        draggable: true
    });
    // Update marker position txt.
    updateMarkerPositionTxt(latLng);
    // Add dragging event listeners.
    google.maps.event.addListener(marker, 'dragstart', function () {
        updateMarkerStateTxt('Inicio de arrastre...');
    });
    google.maps.event.addListener(marker, 'drag', function () {
        updateMarkerStateTxt('Arrastrando...');
        updateMarkerPositionTxt(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'dragend', function () {
        updateMarkerStateTxt('Arrastre terminado');
    });
}
// Onload handler to fire off the app:
google.maps.event.addDomListener(window, 'load', initMap);