<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$act=new Models\Activity();
$seg=new Models\Seguimiento();

$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $seg->__set("id_activity",$id);
    if(!$seg->exists_seguimiento_by_activity()){
        $act->__set("id",$id);
        $result->completed=boolval($act->delete());
    }else{
        $result->completed=false;
    }
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>