<?php

namespace Models;
class Alumnos
{
	private $id;
	private $foto;
	private $nombre;
	private $apellidos;
	private $fecha_nacimiento;
	private $identificacion;
	private $tipo_identificacion;
	private $sexo;
	private $direccion;
	private $password;
	private $tipo;
	private $telefono;
	private $tipo_sangre;
	private $sede;
	private $eps;
	private $prepagada;
	private $email;
	private $departamento_exp;
	private $ciudad_exp;
    private $lugar_nac;
    private $zona;
    

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }

    public function exist(){
        $sql="SELECT * FROM alumnos WHERE identificacion=:identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':identificacion'=>$this->__get("identificacion")
        ));
        if(count($resultado)==0){
            return  false;
        }else{
            return  true;
        }
    }
    public function save(){
        $sql="INSERT INTO alumnos (id, foto, nombre, apellidos, fecha_nacimiento, identificacion, tipo_identificacion, sexo, direccion, password, tipo, telefono, tipo_sangre, sede, eps, prepagada, email, departamento_exp, ciudad_exp, lugar_nac, zona ) VALUES (:id, :foto, :nombre, :apellidos, :fecha_nacimiento, :identificacion, :tipo_identificacion, :sexo, :direccion, :password, :tipo, :telefono, :tipo_sangre, :sede, :eps, :prepagada, :email, :departamento_exp, :ciudad_exp, :lugar_nac, :zona)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':tipo_identificacion'=>$this->__get('tipo_identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':password'=>$this->__get('password'),
			':tipo'=>$this->__get('tipo'),
			':telefono'=>$this->__get('telefono'),
			':tipo_sangre'=>$this->__get('tipo_sangre'),
			':sede'=>$this->__get('sede'),
			':eps'=>$this->__get('eps'),
			':prepagada'=>$this->__get('prepagada'),
            ':email'=>$this->__get('email'),
            ':departamento_exp'=>$this->__get('departamento_exp'),
            ':ciudad_exp'=>$this->__get('ciudad_exp'),
            ':lugar_nac'=>$this->__get('lugar_nac'),
            ':zona'=>$this->__get('zona')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE alumnos SET foto= :foto, nombre= :nombre, apellidos= :apellidos, fecha_nacimiento= :fecha_nacimiento, identificacion= :identificacion, tipo_identificacion= :tipo_identificacion, sexo= :sexo, direccion= :direccion,  tipo= :tipo, telefono= :telefono, tipo_sangre= :tipo_sangre, sede= :sede, eps= :eps, prepagada= :prepagada, email= :email, departamento_exp= :departamento_exp, ciudad_exp = :ciudad_exp, lugar_nac= :lugar_nac, zona=:zona WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':tipo_identificacion'=>$this->__get('tipo_identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':tipo'=>$this->__get('tipo'),
			':telefono'=>$this->__get('telefono'),
			':tipo_sangre'=>$this->__get('tipo_sangre'),
			':sede'=>$this->__get('sede'),
			':eps'=>$this->__get('eps'),
			':prepagada'=>$this->__get('prepagada'),
            ':email'=>$this->__get('email'),
            ':departamento_exp'=>$this->__get('departamento_exp'),
            ':ciudad_exp'=>$this->__get('ciudad_exp'),
            ':lugar_nac'=>$this->__get('lugar_nac'),
            ':zona'=>$this->__get('zona')
        ));
        return $resultado;
    }
    public function update_password(){
        $sql="UPDATE alumnos SET password = :password WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function update_password_automatic(){
        $sql="UPDATE alumnos SET password = :password WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':identificacion'=>$this->__get('identificacion'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function get_user_by_dni(){
        $sql="SELECT email,nombre,apellidos,identificacion FROM alumnos WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':identificacion'=>$this->__get('identificacion')
        ));
        $user->email=$resultado[0]["email"];
        $user->nombre=$resultado[0]["nombre"];
        $user->apellidos=$resultado[0]["apellidos"];
        $user->identificacion=$resultado[0]["identificacion"];
        return $user;
    }
    public function delete(){
        $sql="DELETE FROM alumnos WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }

    public function list_matriculados($start,$finish){
        $result = new\stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT
            matricula.id AS idmatricula,
            grado.nombre AS nombregrado,
            grado.id AS idgrado,
            alumnos.id,
            alumnos.nombre,
            alumnos.apellidos,
            alumnos.telefono,
            alumnos.tipo_sangre,
            alumnos.eps,
            alumnos.foto,
            alumnos.email,
            alumnos.identificacion
        FROM
            matricula
        INNER JOIN alumnos ON matricula.alumnos_id = alumnos.id
        INNER JOIN grado ON matricula.grado_id = grado.id
        WHERE
            matricula.activo = 1 AND (alumnos.nombre LIKE :nombre OR alumnos.apellidos LIKE :apellidos OR  alumnos.identificacion LIKE :identificacion) LIMIT $start, $finish";
        
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM
            matricula
        INNER JOIN alumnos ON matricula.alumnos_id = alumnos.id
        WHERE
            matricula.activo = 1 AND (nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion)";

        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;
        return $result;    
    } 

    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM alumnos WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM alumnos WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;
        return $result;
    }
    public function profile(){
        $sql="SELECT * FROM alumnos WHERE id=:id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':id'=>$this->__get('id')
        ));
        return $resultado1;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }

}
