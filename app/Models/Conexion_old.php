<?php
namespace Models;
class Conexion{
	var $server="localhost";
	var $bd="fi";
    var $user="root";
    var $password="unicundi";
	private $conexion;
	private $total_consultas;
	public function __construct(){
		if(!isset($this->conexion)){
            try{
			    $this->cadenaConexion = "mysql:host=".$this->server.";dbname=".$this->bd;
                $this->conexion = new \PDO($this->cadenaConexion,$this->user,$this->password);
            }catch(PDOException $e){
                echo $e->getMessage();
            }
		}
	}
    public function ejecutarConsulta($sql = "",$values = array()){
        if($sql != ""){
            try {
                $consulta = $this->conexion->prepare($sql);
                $consulta->execute($values);
             //  print_r( $values);
              /*  print_r($consulta);
                print_r($values);
                print_r($consulta->errorInfo());
                */
                $tabla_datos = $consulta->fetchAll(\PDO::FETCH_ASSOC);
                return $tabla_datos;
            } catch(PDOExecption $e) {
                write_log($e->getMessage(),"Mysql");
            }
        }else{
            return 0;
        }
    }
    public function lastId($id){
        return $this->conexion->lastInsertId($id);
    }
    /**
     * Metodo que nos permitira obtener un entero de las tablas afectadas, 0 indica que no
     * paso nada.
     * Usamos PDO en PHP.
     * @param $sql Sentencia SQL.
     * @param $values Arreglo de bindValues de PDO para la consulta SQL.
     * @return $numero_tablas_afectadas Numero entero de las tablas afectadas de 0 a N.
     * */
    public function ejecutarActualizacion($sql = "",$values = array()){
        if($sql != ""){
            try{
                $consulta = $this->conexion->prepare($sql);
                $consulta->execute($values);
            /*    print_r($consulta);
                print_r($values);
                print_r($consulta->errorInfo());*/
                $numero_tablas_afectadas = $consulta->rowCount();
                return $numero_tablas_afectadas;
            } catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }else{
            return 0;
        }
    }

    public function procesar($sql = "",$values = array()){
        if($sql != ""){
            $consulta = $this->conexion->prepare($sql);
            $consulta->execute($values);
            $numero_tablas_afectadas = $consulta->insert_id;
            return $numero_tablas_afectadas;
        }else{
            return 0;
        }
    }
    public function extract_dates($type){
		switch ($type) {
			case 'POST':
            try{    
                foreach($_POST as $nombre_campo => $valor){
                    $asignacion = "\$GLOBALS[\"" . $nombre_campo . "\"]='" . $valor . "';";
                    eval($asignacion);
                }
            }catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
			break;
			case 'GET';
			try{
                foreach($_GET as $nombre_campo => $valor){
                    $asignacion = "\$GLOBALS[\"" . $nombre_campo . "\"]='" . $valor . "';";
                    eval($asignacion);
			    }
            }catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
			
			break;
		}
	}
}
?>