elements = {
    
	"#n": {
		"obligatorio": true,
		"msg": "Por favor ingrese la semana"
	},
	"#unidad": {
		"obligatorio": true,
		"msg": "Por favor ingrese la unidad"
    },
    "#unidad_tematica": {
		"obligatorio": true,
		"msg": "Por favor ingrese la unidad temática"
	},
	"#aprendizaje": {
		"obligatorio": true,
		"msg": "Por favor el objetivo de aprendizaje"
	}
}
Plan = new Vue({
    el: '#Plan',
    data: {
        id: 0,
        operation: "Agregar Plan",
        competencia:["Cognitivo","Procedimental","Axiológico","Actitudinal","Comunicativo","Manejo de segunda lengua"],
        list: null,
        list2: null,
        list3: null,
        upload:true,
        preload_list: false,
        list_archivos:null,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        preload_archivos:false,
        optionsSelect1: [{
            text: "Cargando..",
            value: "0"
        }],
        optionsSelect2:[{text:"Cargando..",value:"0"}],
        grado:"",
        asignacion:"",
        n:"",
        total:0,
        rowspan:0,
        totalpor:0,
        status_rubrica:"Rubrica en creación",
        obj_actividad:null,
    },
    created: function () {
        this.onlist(1);
        this.onListGrados();
    },
    methods: {
        onlist_archivos: function (data) {
            var url = 'Services/Actividades_pdf/list.php?plan_id=' + $("#plan_id_archivo").val() + '&nombre=' + $("#search_archivos").val();
            var self = this;
            self.preload_archivos=true;
            $.get(url, function (data) {
                self.list_archivos = data;
                self.preload_archivos=false;
            });
        },
        update_archivos:function(data){
            $("#id_archivo").val(data.id);
            $("#archivo").val(data.nombre);
            $("#nombre_archivo").val(data.url);
        },
        ondeleted_archivo: function (data) {
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                var url = 'Services/Actividades_pdf/delete.php?id=' + data.id + '&url=' + data.url;
                var self = this;
                self.list_archivos = true;
                $.get(url, function (data) {
                    self.onlist_archivos(1);
                });
            });
        },
        sum:function(value){
            this.totalpor=this.totalpor+parseInt(value);
            return value;
        },
        archivos:function(data){
            $("#plan_id_archivo").val(data.id);
            this.onlist_archivos(1);
        },
        actividades:function(data){
            this.obj_actividad=data;
            this.grado=$("#grado_id option:selected").text();
            this.asignacion=$("#asignaciones_id option:selected").text();
            this.aprendizaje=data.aprendizaje;
            this.n=data.n;
            $("#form_Actividades #plan_id").val(data.id);
            this.onlistActividades();
            console.log(data)
        },
        diseno_curricular:function(data){
            $("#form_Rubrica #plan_id").val(data.id);
            this.onlistRubricas(data.id);
        },
        ondeletedActivity: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Actividades/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlistActividades();
                            }
                            if (!data.completed) {
                                swal('Operación fallida',
                                'No hemos podido eliminar el registro, debido a que algunos alumnos ya tienen asociado esta actividad',
                                'question'
                                );
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdateActivity: function (data) {
            this.id = data.id;
            this.operation = "Modificar Actividades";
            $("#plan_id").val(data.plan_id);
			$("#actividad").val(data.actividad);
			$("#tiempo").val(data.tiempo);
			
        },
        onListGrados: function () {
            var url = 'Services/Grados/select.php';
            var self = this;
            $.get(url, function (data) {
                datos = data.data;
                self.optionsSelect1 = [];
                for (var i = 0; i < datos.length; i++) {
                    self.optionsSelect1.push({
                        text: datos[i].nombre,
                        value: datos[i].id
                    });
                }
                setTimeout(function(){
                    self.onListAsignaciones();
                },500);
            });
        },
        onListAsignaciones:function(){
            var url = 'Services/Asignaciones/list_select_by_grado.php?grado_id='+$("#grado_id").val();
            var self=this;
            $.get(url, function (data) {
                datos=data.data;
                self.optionsSelect2=[];
                for (var i=0; i<datos.length; i++){
                    self.optionsSelect2.push({text:datos[i].nombre,value:datos[i].id});
                }
                setTimeout(function(){
                    self.onlist(1);
                },500);
                
            });
        },
        ondeletedRubrica: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Rubrica/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlistRubricas($("#form_Rubrica #plan_id").val());
                            }
                            if (!data.completed) {
                                swal('Operación fallida',
                                'No hemos podido eliminar el registro, debido a que algunos alumnos ya tienen asociado esta rúbrica',
                                'question'
                                );
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdateRubrica: function (data) {
            this.id = data.id;
            this.operation = "Modificar Rubrica";
            $("#form_Rubrica #plan_id").val(data.plan_id);
			$("#competencia").val(data.competencia);
			$("#criterio_evaluacion").val(data.criterio_evaluacion);
			$("#peso").val(data.peso);
			/*$("#mejorar").val(data.mejorar);
			$("#basico").val(data.basico);
			$("#alto").val(data.alto);
			$("#superior").val(data.superior);*/
        },
        onlistRubricas: function (page) {
            var self=this;
            var url = 'Services/Rubrica/list.php?paginas=15&page=1&plan_id='+page;
            self.totalpor=0;
            $.get(url, function (data) {
                console.log(data);
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list3 = data["data"];
                    var n=self.list3.length;
                    for(var i=0; i<n; i++){
                        self.totalpor=parseInt(self.list3[i].peso)+self.totalpor;
                    }
                    self.preload_list = false;
                }
                if(n==self.competencia.length){
                    self.status_rubrica="Rubrica en terminada"
                }
                else{
                    self.status_rubrica="Rubrica en creación"
                }

                self.analitics_competencia()
                
            });
        },
        analitics_competencia: function () {
            var e = this;
            var limit = e.list3.length;
            $("#competencia").attr("access", "true");
            $("#competencia").css("background-color", "#FFF");
            $("#competencia").css("color", "#67757c");
            for (var j = 0; j < limit; j++) {
                console.log("ele " + j)
                if (e.list3[j].competencia == $("#competencia").val()) {
                    $("#competencia").attr("access", "false");
                    $("#competencia").css("background-color", "green");
                    $("#competencia").css("color", "#FFF");
                    break;
                }
            }
            
            
        },
        get_competencia:function(id){
            return this.competencia[id];
        },
        onlistActividades: function () {
            var self = this;
            var url = 'Services/Actividades/list.php?plan_id=' + this.obj_actividad.id;
            self.total=0;
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.rowspan=data.length;
                    for (var i=0; i<self.rowspan; i++){
                        self.total+=parseInt(data[i].tiempo);
                        
                    }
                    self.total=(self.total/60)+" Horas"
                    self.list2 = data;
                }
            });
        },
        onsaveRubrica: function (data) {
            var self = this;
            if(($("#competencia").attr("access")=="true" && data=="new") ||	data=="update"){
                save({
                    url: "Services/Rubrica/save.php?" + $("#form_Rubrica").serialize(),
                    result: function (data) {
                        console.log(data);
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Rubrica se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;

                        $("#criterio_evaluacion").val("");
						$("#peso").val("");
						/*$("#mejorar").val("");
						$("#basico").val("");
						$("#alto").val("");
						$("#superior").val("");*/
						
                        self.onlistRubricas($("#form_Rubrica #plan_id").val()) ;
                        self.operation = "Agregar Rubrica";
                    }
                });
            }else{
                swal('¡Error!',
                'Solo puedes ingresar una competencia del mismo tipo',
                'error' );
            }
            
        },
        onsaveActivity: function () {
            var self = this;
                save({
                    url: "Services/Actividades/save.php?" + $("#form_Actividades").serialize(),
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Actividades se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                        self.onlistActividades();
						$("#actividad").val("");
						$("#tiempo").val("");
						
                        self.onlist(1);
                        self.operation = "Agregar Actividades";
                    }
                });
            
        },
        ondeleted: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Plan/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Plan";
            $("#grado_id").val(data.grado_id);
            $("#asignaciones_id").val(data.asignaciones_id);
            $("#derechos").val(data.derechos);
            $("#unidad").val(data.unidad);
			$("#unidad_tematica").val(data.unidad_tematica);
			$("#n").val(data.n);
			$("#aprendizaje").val(data.aprendizaje);
        },
        onlist: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Plan/list.php?paginas=15&page=' + page + '&asignaciones_id=' + $('#asignaciones_id').val() + '&derechos=' + $('#search').val() + '&aprendizaje=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    url: "Services/Plan/save.php?" + $("#form_Plan").serialize(),
                    result: function (data) {
                        console.log(data);
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Plan se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                		$("#derechos").val("");
                		$("#n").val("");
						$("#aprendizaje").val("");
						$("#unidad").val("");
						$("#unidad_tematica").val("");
				        self.onlist(1);
                        self.operation = "Agregar Plan";
                    }
                });
            }
        }
    }
});

function upload() {
   
    formdata = false;
    //Revisamos si el navegador soporta el objeto FormData
    if (window.FormData) {
        formdata = new FormData();
    }
    self = document.getElementById('files');
    var ext = $( self  ).val().split('.');
    ext=(ext[ext.length-1].toLowerCase());
    //si el id es diferente de cero se trata de una actualizacion y se desbloquea que el archivo sea obligatorio//
    if ($( self ).val() != '' || $("#id_archivo").val() != '0') {
        //se valida si es pdf pasa
        //si se tratara de una actualizacion se verificara que este vacio o que sea pdf en la otra condicion//
        if(ext == "pdf" || ( $("#id_archivo").val() != '0' && $( self ).val() == '' )){
            Plan.upload=false;
            var i = 0, len = self.files.length, img, reader, file;
            //document.getElementById('response').innerHTML = 'Subiendo...';
            //Si hay varias imágenes, las obtenemos una a una
            for (; i < len; i++) {
                file = self.files[i];
        
                //Una pequeña validación para subir imágenes
                //if(!!file.type.match(/image.*/)){
                //Si el navegador soporta el objeto FileReader
                if (window.FileReader) {
                    reader = new FileReader();
                    //Llamamos a este evento cuando la lectura del archivo es completa
                    //Después agregamos la imagen en una lista
                    reader.onloadend = function (e) {
                        //Plan.onlist_archivos(1);
                    };
                    //Comienza a leer el archivo
                    //Cuando termina el evento onloadend es llamado
                    reader.readAsDataURL(file);
                }
        
                //Si existe una instancia de FormData
                if (formdata){
                    //Usamos el método append, cuyos parámetros son:
                    //name : El nombre del campo
                    //value: El valor del campo (puede ser de tipo Blob, File e incluso string)
                    formdata.append('files[]', file);
                    if(file.size>2000000){
                        formdata=false;
                    }
                    
                }
                //}
            }
            //Por último hacemos uso del método proporcionado por jQuery para hacer la petición ajax
            //Como datos a enviar, el objeto FormData que contiene la información de las imágenes
            if (formdata) {
                formdata.append('nombre', $("#archivo").val());
                formdata.append('plan_id', $("#plan_id_archivo").val());
                formdata.append('id_archivo', $("#id_archivo").val());
                formdata.append('nombre_archivo', $("#nombre_archivo").val());
                $.ajax({
                    url: 'Services/Actividades_pdf/save.php',
                    type: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        Plan.upload=true;
                        if(res.completed){
                            $("#files").val("");
                            $("#archivo").val("");
                            $("#id_archivo").val("");
                            $("#nombre_archivo").val("");
                           
                            Plan.onlist_archivos(1);
                            swal({
                                type: 'success',
                                title: 'Su archivo se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });   
                        }else{
                            swal('¡Error!',
                            'No pudimos procesar tu petición por favor consulte con el administrador',
                            'error' );  
                        }
                    }
                });
            }else{
                Plan.upload=true;
                swal('¡Error!',
                'El archivo supera los 2 mb.',
                'error' );
            }
        }else{
            swal('¡Error!',
            'Solo puedes ingresar archivos pdf',
            'error' );
            Plan.upload=true;
        }
    }else{
        swal('¡Error!',
            'No indicaste el pdf a subir',
            'error' );
            Plan.upload=true;
    }

    
}