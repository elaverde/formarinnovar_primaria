<?php

namespace Models;
class Rubrica
{
	private $id;
	private $plan_id;
	private $competencia;
	private $criterio_evaluacion;
	private $peso;
	private $mejorar;
	private $basico;
	private $alto;
	private $superior;
	private $profesor;
	private $dia;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO rubrica (id, plan_id, competencia, criterio_evaluacion, peso, mejorar, basico, alto, superior,profesor,dia) VALUES (:id, :plan_id, :competencia, :criterio_evaluacion, :peso, :mejorar, :basico, :alto, :superior, :profesor, :dia)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':plan_id'=>$this->__get('plan_id'),
			':competencia'=>$this->__get('competencia'),
			':criterio_evaluacion'=>$this->__get('criterio_evaluacion'),
			':peso'=>$this->__get('peso'),
			':mejorar'=>$this->__get('mejorar'),
			':basico'=>$this->__get('basico'),
			':alto'=>$this->__get('alto'),
            ':profesor'=>$this->__get('profesor'),
            ':dia'=>$this->__get('dia'),
			':superior'=>$this->__get('superior')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE rubrica SET plan_id= :plan_id, competencia= :competencia, criterio_evaluacion= :criterio_evaluacion, peso= :peso, mejorar= :mejorar, basico= :basico, alto= :alto, superior= :superior, profesor=:profesor, dia=:dia WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':plan_id'=>$this->__get('plan_id'),
			':competencia'=>$this->__get('competencia'),
			':criterio_evaluacion'=>$this->__get('criterio_evaluacion'),
			':peso'=>$this->__get('peso'),
			':mejorar'=>$this->__get('mejorar'),
			':basico'=>$this->__get('basico'),
			':alto'=>$this->__get('alto'),
            ':superior'=>$this->__get('superior'),
            ':profesor'=>$this->__get('profesor'),
            ':dia'=>$this->__get('dia')
        ));
        return $resultado;
    }
    public function get_rubrica_by_plan(){
        $sql="SELECT
            rubrica.id,
            rubrica.alto,
            rubrica.basico,
            rubrica.competencia,
            rubrica.criterio_evaluacion,
            rubrica.mejorar,
            rubrica.peso,
            rubrica.plan_id,
            rubrica.superior
        FROM
            plan
        INNER JOIN rubrica ON rubrica.plan_id = plan.id
        WHERE
            rubrica.plan_id = :plan_id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id')
        ));
        return $resultado1;
    }
    public function delete(){
        $sql="DELETE FROM rubrica WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new\stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM rubrica WHERE  plan_id = :plan_id LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id')
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM rubrica WHERE  plan_id = :plan_id";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':plan_id'=>$this->__get('plan_id')
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}