elements = {
	"#foto": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Foto"
	},
	"#nombre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Nombre"
	},
	"#apellidos": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Apellidos"
	},
	"#fecha_nacimiento": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Fecha de nacimiento"
	},
	"#identificacion": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Identificación"
	},
	"#sexo": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Sexo"
	},
	"#direccion": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Dirección"
	},
	"#password": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Password"
	},
	"#telefono": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Teléfono"
	},
	"#tipo_sangre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Tipo de sangre"
	}
}
var Acudientes = new Vue({
    el: '#Acudientes',
    data: {
        id: 0,
        operation: "Agregar Acudiente",
        preload_list: false,
        list: null,
        list2: null,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
    },
    created: function () {
        this.onlist(1);
    },
    methods: {
        onupdatepassword:function(){
            if($("#password1").val()!=""){
                if($("#password2").val()==$("#password1").val()){
                    save({
                        type: "GET",
                        url: "Services/Acudientes/update_password.php?password="+$("#password1").val()+"&id="+$("#fi_id").val() ,
                        result: function (data) {
                            if (data.completed) {
                                swal({
                                    type: 'success',
                                    title: 'Se ha almacenado correctamente',
                                    showConfirmButton: false,
                                    timer: 3500
                                });
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }else{
                    swal({
                        type: 'question',
                        title: 'Revisa tu contraseñas no coinciden con la confirmación',
                        showConfirmButton: false,
                        timer: 3500
                    });
                }
            }else{
                swal({
                    type: 'error',
                    title: 'Ingresa una contraseña',
                    showConfirmButton: false,
                    timer: 3500
                });
            }
        },
        activate_cam: function () {
            $("#web-cam").attr("src", "web-cam/foto.html");
        },
        asignar_acudiente:function(i){
            $("#textalum").val(i.nombre+" "+i.apellidos);
            $("#dnialumno").val(i.id);
            $('#exampleModal2').modal('hide');
        },
        onupdate: function (data) {
            this.id = data.id;
            this.operation = "Modificar Acudientes";
            $("#foto").val(data.foto);
            $("#imgfoto").attr("src",data.foto);
			$("#nombre").val(data.nombre);
			$("#apellidos").val(data.apellidos);
			$("#fecha_nacimiento").val(data.fecha_nacimiento);
			$("#identificacion").val(data.identificacion);
			$("#sexo").val(data.sexo);
			$("#direccion").val(data.direccion);
			$("#parentesco").val(data.parentesco);
			$("#password").val(data.password);
			$("#telefono").val(data.telefono);
            $("#tipo_sangre").val(data.tipo_sangre);
            $("#passwordbox").hide();
            $("#email").val(data.email);
        },
        onlist: function (page) {
            var self = this;
            var url = 'Services/Acudientes/get_profile.php?id='+$("#fi_id").val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.onupdate(data[0]);
                }
                $("#botonFoto").on({
                    click: function () {
                        window.frames['web-cam'].take_snapshot(function (url) {
                            $("#foto").val(url);
                            $("#imgfoto").attr("src", url);
                            $("#status_picture").css("color", "green");
                            $("#web-cam").attr("src", "#");
                        });
                    }
                });
                $("#logo2").change(function (evt) {
                    handleFileSelect(evt, "logo", function (img) {
                        $("#imgfoto").attr("src", "data:image/jpg;base64," + img);
                        $("#foto").val("data:image/jpg;base64," + img);
                    });
                });
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    data: $("#form_Acudientes").serialize(),
                    type: "post",
                    url: "Services/Acudientes/save.php" ,
                    result: function (data) {
                        console.log(data);
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'El Acudientes se ha almacenado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                        self.onlist(1);
                        self.operation = "Agregar Acudientes";
                    }
                });
            }
        }
    }
});