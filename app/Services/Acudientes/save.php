<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
require_once "../../Models/Messages.php";
Config\Autoload::run();
$db= new Models\Conexion();
$acu=new Models\Acudientes();
$aac=new Models\Asignacion_Acudiente();
$info=new Models\Info_System();
$mail=new Messages();
$result=  new stdClass();
if($info->session_active()){
	$db->extract_dates("POST");
	$acu->__set("foto",$foto);
	$acu->__set("nombre",$nombre);
	$acu->__set("apellidos",$apellidos);
	$acu->__set("fecha_nacimiento",$fecha_nacimiento);
	$acu->__set("identificacion",$identificacion);
	$acu->__set("sexo",$sexo);
	$acu->__set("direccion",$direccion);
	$acu->__set("password",md5($password));
	$acu->__set("parentesco",$parentesco);
	$acu->__set("telefono",$telefono);
	$acu->__set("email",$email);
	
    if($id==0) {
			if(!$acu->exist()){
				$acu->__set("id", null);
				$data=$acu->save();
				$result->completed = boolval($data["affected"]);
				$aac->__set("alumnos_id",$dnialumno);
				$aac->__set("acudiente_id",$data["id"]);
				$aac->save();
				$mail->setPass($info->__get("pass"));
				$mail->setEmail($info->__get("email"));
				$mail->setNamefrom($info->__get("from"));
				$mail->setAsunto("Bienvenido a la familia Formar Innovar");
				$mail->setMsg('<img width="700" src="https://formarinnovar.com/Services/images/logo_web-01.png"><h1>Estimado usuario,</h1><p style="font-size: 20px;">Nos complace darle la más cordial bienvenida al gran familia del Colegio campestre Formar Innovar.</p><i><b>“Un Lugar para ser Feliz”</b></i><p style="font-size: 16px;">Para el ingreso al sistema se generaron un usuario y contraseña</p><b>Usuario:</b> '.$identificacion.'<br><b>Password:</b> '.$password.' <br><br><a href="https://formarinnovar.com/Services/app/" target="_blank">Entrar</a>');
				$mail->setName($nombre." ".$apellidos);
				$mail->setPara($email);
				$mail->send($mail);
				echo json_encode($result);
			}else{
				$result->completed = "exist";
				echo json_encode($result);
			}
    }else{
        $acu->__set("id", $id);
				$result->completed = boolval($acu->update());
				echo json_encode($result);
    }
		
	
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>