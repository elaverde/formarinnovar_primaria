elements = {
	"#grado_id": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Grado"
	},
	"#nombre": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Nombre Asignación"
	},
	"#horas": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Horas Semanales"
	},
	"#horas_min": {
		"obligatorio": true,
		"msg": "Por favor ingrese el Horas minimas diarias"
	}
}
var Asignaciones = new Vue({
    el: '#Asignaciones',
    data: {
        id: 0,
        operation: "Agregar Asignaciones",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        link:"#",
        optionsSelect1:[{text:"Cargando..",value:"0"}],
        optionsSelect2:[{text:"Cargando..",value:"0"}]
    },
    created: function () {
        this.onListGrados();
    },
    methods: {
        ondeleted: function (data) {
            var self=this;
            swal({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminarlo!'
            }).then((result) => {
                if (result.value) {
                    save({
                        url: "Services/Asignaciones/delete.php?id=" + data.id,
                        result: function (data) {
                            if (data.completed) {
                                swal('¡Eliminado!',
                                    'Tu registro ha sido eliminado.',
                                    'success'
                                );
                                self.onlist(1);
                            }
                            if (data.completed == "expired") {
                                session_expired();
                            }
                        }
                    });
                }
            });
        },
        onupdate: function (data) {
            console.log(data);
            this.id = data.id;
            this.operation = "Modificar Asignaciones";
            $("#grado_id").val(data.grado_id);
            $("#nombre").val(data.nombre);
        	$("#tipo").val(data.tipo);
            $("#grupo").val(data.grupo);
            this.link="Syllabus/"+data.syllabus;
			$("#horas").val(data.horas);
			$("#horas_min").val(data.horas_min);
			$("#syllabus").val(data.syllabus);
        },
        onListGrados:function(){
            var url = 'Services/Grados/select.php';
            var self=this;
            $.get(url, function (data) {
                datos=data.data;
                self.optionsSelect1=[];
                for (var i=0; i<datos.length; i++){
                    self.optionsSelect1.push({text:datos[i].nombre,value:datos[i].id});
                }
                setTimeout(function(){
                    self.onlist(1);
                    self.onListGrupos();
                },500);
                
            });
        },
        onListGrupos:function(){
            var url = 'Services/Grupos_Electivos/list_select_by_grado.php?grado_id='+$("#grado_id").val();
            var self=this;
            $.get(url, function (data) {
                datos=data;
                self.optionsSelect2=[];
                for (var i=0; i<datos.length; i++){
                    self.optionsSelect2.push({text:datos[i].nombre,value:datos[i].id});
                }
            });
        },
        onlist: function (page) {
            this.onListGrupos();
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Asignaciones/list.php?paginas=15&page=' + page + '&nombre=' + $('#search').val()+"&grado_id="+$("#grado_id").val();
            console.log(url);
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                Atlas('files').Upload_file(
                    {
                        url:"Services/Asignaciones/save.php",
                        data:[
                            {key:"id",value:$("#id").val()},
                            {key:"grado_id",value:$("#grado_id").val()},
                            {key:"nombre",value:$("#nombre").val()},
                            {key:"tipo",value:$("#tipo").val()},
                            {key:"syllabus",value:$("#syllabus").val()},
                            {key:"horas",value:$("#horas").val()},
                            {key:"grupo",value:$("#grupo").val()},
                            {key:"horas_min",value:$("#horas_min").val()}
                        ],
                        rules:{
                            required_file:false,
                            ext:["pdf"],
                            size:2000000
                        },
                        events:{
                            On_Success:function(data){
                                console.log(data);
                                if(data.completed){
                                    swal({
                                        type: 'success',
                                        title: 'El Asignaciones se ha almacenado correctamente',
                                        showConfirmButton: false,
                                        timer: 3500
                                    });
                                    self.id = 0;
                                    $("#nombre").val("");
                                    $("#syllabus").val("");
                                    $("#files").val("");
                                    $("#horas").val("");
                                    $("#horas_min").val("");
                                    self.onlist(1);
                                    self.operation = "Agregar Asignaciones";
                                }
                                if (data.completed == "expired") {
                                    session_expired();
                                }
                            },
                            On_Exceeded_Size:function(){
                                swal('¡Error!',
                                'El archivo supera los 2 mb.',
                                'error' );
                            },
                            On_Starting:function(){
                                
                            },
                            On_Fail_Extension:function(){
                                swal('¡Error!',
                                'Solo puedes ingresar archivos pdf',
                                'error' );
                            },
                            On_Fail_File:function(){
                                swal('¡Error!',
                                'No indicaste el pdf a subir',
                                'error' );
                            }
                        }
                    }
                );
            }
        }
    }
});