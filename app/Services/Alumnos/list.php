<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$alu=new Models\Alumnos();
$info= new Models\Info_System();

$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$alu->__set("nombre",$nombre);
	$alu->__set("apellidos",$apellidos);
	$alu->__set("identificacion",$identificacion);
    echo json_encode($alu->list($page,$paginas));
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>