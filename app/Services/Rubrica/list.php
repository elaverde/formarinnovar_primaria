<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$rub=new Models\Rubrica();
$info= new Models\Info_System();

$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
	$rub->__set("plan_id",$plan_id);
    echo json_encode($rub->list($page,$paginas));
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>