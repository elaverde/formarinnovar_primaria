<?php
namespace Models;
class Grados
{
	private $id;
	private $nombre;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO grado (id,nombre) VALUES (:id, :nombre)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE grado SET nombre= :nombre WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM grado WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list_select(){
        $result=  new  \stdClass();
        $sql="SELECT * FROM grado WHERE 1";
        $resultado1 = $this->con->ejecutarConsulta($sql);
        $result->data=$resultado1;
        return $result;
    }
    public function list($start,$finish){
        $result=  new\stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM grado WHERE nombre LIKE :nombre LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM grado WHERE nombre LIKE :nombre ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;
        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
}