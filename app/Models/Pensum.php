<?php
namespace Models;
class Pensum
{
	private $id;
	private $matricula_id;
	private $id_asignacion;
	private $nombre_asignacion;
	private $tiempo;
	private $completa;
	private $grupo;
	private $tiempo_esperado;
    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function save(){
        $sql="INSERT INTO pensum (id, matricula_id, id_asignacion, nombre_asignacion, tiempo, completa, tiempo_esperado, grupo) VALUES (:id, :matricula_id, :id_asignacion, :nombre_asignacion, :tiempo, :completa, :tiempo_esperado, :grupo)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':matricula_id'=>$this->__get('matricula_id'),
			':id_asignacion'=>$this->__get('id_asignacion'),
			':nombre_asignacion'=>$this->__get('nombre_asignacion'),
			':tiempo'=>$this->__get('tiempo'),
			':completa'=>$this->__get('completa'),
			':tiempo_esperado'=>$this->__get('tiempo_esperado'),
			':grupo'=>$this->__get('grupo'),
        ));
        return $resultado;
    }
    public function update_hora_catedra(){
        $sql="UPDATE pensum
                INNER JOIN matricula ON pensum.matricula_id = matricula.id
              SET pensum.tiempo = :tiempo
              WHERE
                matricula.id = :matricula_id
              AND pensum.id_asignacion = :id_asignacion";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':matricula_id'=>$this->__get('matricula_id'),
			':id_asignacion'=>$this->__get('id_asignacion'),
			':tiempo'=>$this->__get('tiempo')
        ));
        return $resultado;
    }
    public function onlist(){
        $sql="SELECT * FROM pensum WHERE matricula_id = :matricula_id ";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':matricula_id'=>$this->__get('matricula_id')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM pensum WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }    
}