elements = {
	"#idalumno": {
		"obligatorio": true,
		"msg": "Por favor seleccioné el alumno"
	}
}
var Plan_Ejecucion = new Vue({
    el: '#Plan_Ejecucion',
    data: {
        id: 0,
        alumno:null,
        operation: "Matrícular",
        competencia:["Cognitivo","Procedimental","Axiológico","Actitudinal","Comunicativo","Manejo de segunda lengua"],
        list: null,
        list2: null,
        list3: null,
        preload_progreso:false,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        prev2: false,
        next2: false,
        pages2: null,
        results2: 0,
        page2: 1,
        show2: 0,
        grado:0,
        optionsSelect2:[{text:"Cargando..",value:"0"}],
        seguimiento:null,
        evaluacion:null,
        placomplete:null,
        R_total:0,
        rowspan:0,
        total:0,
        area:null,
        namealum: "Nombre",
        dnialum: "Identificación",
        nombregrado:null,
        totalseguimiento:0,
        totalevaluacion:0
    },
    created: function () {
        this.onlistAlumnos(1);

    },
    
    methods: {
        elaborate_plan:function(data,action){

            var url = 'Services/Actividades/generar_plan_seguimiento_rubrica.php?plan_id='+data.id+"&matricula_id="+this.alumno.idmatricula;
            var self=this;
            $.get(url, function (data) {
                self.placomplete=data;
                self.seguimiento=data.seguimiento;
                self.evaluacion=data.evaluacion;
                self.rowspan=self.placomplete.actividades.length;
                if(action==1){
                    self.area=$("#asignaciones_id option:selected").text();
                }
                if(action==2){
                    setTimeout(function(){
                        self.evaluar_rubrica();
                    },500);
                }
            });
        },
        get_competencia:function(id){
            return this.competencia[id];
        },
        guardar_seguimiento: function () {
            var self = this;
            var url = 'Services/Seguimiento/save.php?'+$("#form_Actividades").serialize();
            save({
                url: url,
                result: function (data) {
                    console.log(data);
                    swal('Almacenado',
                    'Tu registro ha sido almacenado correctamente.',
                    'success'
                    );
                }
            });
        },
        evaluar_rubrica:function(){
            var n=this.placomplete.evaluacion.length;
            var total=0;
            $("#ejecucion_plan_id").val(this.placomplete.evaluacion[0].ejecucion_plan_id);
            for(var i=0; i<n; i++){
                var value = isNaN(parseInt($("#c"+i+"s").val())) ? 0 : parseInt($("#c"+i+"s").val());
                total=total+(value*(parseInt($("#c"+i).text())/100));
            }
            $("#R_total").text(total.toFixed(2));
            $("#notafinal").val(total.toFixed(2));

            


        },
        get_dates_actividad:function(id,proposito){
            var data=this.placomplete.actividades;
            var n=data.length;
          //
            for(var i=0; i<n; i++){
                if(data[i].id==id){
                    return data[i][proposito];
                }
            }
        },
        get_dates_rubrica:function(id,proposito){
            var data=this.placomplete.rubricas;
            var n=data.length;
          //  this.rowspan=n;
            for(var i=0; i<n; i++){
                if(data[i].id==id){
                    return data[i][proposito];
                }
            }
        },
        guardar_rubrica:function(){
            var self = this;
            var url = 'Services/Evaluacion/save.php?'+$("#form_Rubrica").serialize();
            save({
                url: url,
                result: function (data) {
                    console.log(data);
                    swal('Almacenado',
                    'Tu registro ha sido almacenado correctamente.',
                    'success'
                    );
                }
            });
        },
        //select asinaciones importante para listar modulo flotante//
        onListAsignaciones:function(){
            var url = 'Services/Asignaciones/list_select_by_grado.php?grado_id='+this.grado;
            var self=this;
            $.get(url, function (data) {
                datos=data.data;
                self.optionsSelect2=[];
                for (var i=0; i<datos.length; i++){
                    self.optionsSelect2.push({text:datos[i].nombre,value:datos[i].id});
                }
                setTimeout(function(){
                    self.area=$("#asignaciones_id option:selected").text();
                    self.onlistPlansRealizados(1);
                    self.onlistPlans(1);
                },1000);
                
            });
        },
        onlistPlansRealizados: function (page) {
            var self=this;
            self.area=$("#asignaciones_id option:selected").text();
            self.preload_progreso=true;
            var url = 'Services/Plan/list_realizados.php?paginas=150&page=' + page + '&asignaciones_id=' + $('#asignaciones_id').val()+"&matricula_id="+this.alumno.idmatricula ;
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list3 = data["data"];
                    self.preload_progreso=false;
                }
            });
        },
        //listado de plan modulo flotante//
        onlistPlans: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page2) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page2) - 1
            };
            var url = 'Services/Plan/list.php?paginas=15&page=' + page + '&asignaciones_id=' + $('#asignaciones_id').val() + '&derechos=' + $('#search2').val() + '&aprendizaje=' + $('#search2').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list2 = data["data"];
                    self.results2 = data.result;
                    self.page2 = Math.ceil(data.page / 15);
                    self.show2 = data.show2;
                    var tab = paginador(self.page2, self.show2, self.results2);
                    self.prev2 = tab.prev2;
                    self.next2 = tab.next2;
                    self.pages2 = tab.pages2;
                    self.preload_list = false;
                }
            });
        },
        onlist: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Plan/list.php?paginas=15&page=' + page + '&asignaciones_id=' + $('#asignaciones_id').val() + '&derechos=' + $('#search').val() + '&aprendizaje=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onlistAlumnos: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Alumnos/list_matriculados.php?paginas=15&page=' + page + '&nombre=' + $('#search').val() + '&apellidos=' + $('#search').val() + '&identificacion=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onListPlan: function (item) {
            var self=this;
            this.alumno=item;
            this.grado=item.idgrado;
            this.nombregrado=item.nombregrado;
            $("#idalumno").val(item.id);
            this.namealum = "Nombres: " + item.nombre + " " + item.apellidos;
            this.dnialum = "Identificación: " + item.identificacion;
            $("#avatar_alumno").attr("src", item.foto);
            this.onListAsignaciones();
            $.get("Services/Seguimiento/get_count_seguimiento.php?matricula_id="+this.alumno.idmatricula , function (data) {
                self.totalseguimiento=data.count;
            });
            $.get("Services/Evaluacion/get_count_evaluacion.php?matricula_id="+this.alumno.idmatricula , function (data) {
                self.totalevaluacion=data.count;
            });
        },
        onsave: function () {
            var self = this;
            
            
        }
    }
});