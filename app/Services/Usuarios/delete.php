<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$adm=new Models\Admin();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $adm->__set("id",$id);
    $result->completed=boolval($adm->delete());
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>