var app = new Vue({
    el: '#main-wrapper',
    data: {
        module: 'Inicio',
        preload: false,
        app_name: "© 2018 Oficina Sistemas"
    },
    methods: {
        loadmodule: function (data) {
            var self = this;
            this.preload = true;
            this.module = data.titulo;
            $.ajax({
                url: data.url + "?ramdon=" + Math.random(),
                cache: false,
                dataType: "html",
                success: function (html) {
                    self.preload = false;
                    setTimeout(function () {
                        $("#vistas").html(html);
                        data.excecute();
                    }, 50)
                }
            });
        }
    }
});

$("a").on({
    click: function () {
        var folder = "Views/";
        var module = {};
        if ($(this).attr("data-ng-modules") != null) {
            switch ($(this).attr("data-ng-modules")) {
                case "add_grados":
                    module.excecute = function () {
                        app.module = "Grados";
                    }
                    module.url = folder + "Grados/Grados.html";
                    break;
                case "add_asignacion":
                    module.excecute = function () {
                        app.module = "Asignación";
                    }
                    module.url = folder + "Asignaciones/Asignaciones.html";
                    break;
                case "add_plan":
                    module.excecute = function () {
                        app.module = "Temas";
                    }
                    module.url = folder + "Plan/Plan.html";
                    break;
                case "add_alumnos":
                    module.excecute = function () {
                        app.module = "Alumnos";
                    }
                    module.url = folder + "Alumnos/Alumnos.html";
                    break;
                case "add_matricula":
                    module.excecute = function () {
                        app.module = "Matrículas";
                    }
                    module.url = folder + "Matriculas/Matriculas.html";
                    break;
                case "add_asignar_catedra":
                    module.excecute = function () {
                        app.module = "Asignar Cátedra";
                    }
                    module.url = folder + "Asignar_Catedra/Asignar_Catedra.html";
                    break;
                case "add_profesor":
                    module.excecute = function () {
                        app.module = "Crear Horario";
                    }
                    module.url = folder + "Profesores/Profesores.html";
                    break;
                case "add_horario":
                    module.excecute = function () {
                        app.module = "Crear Instructor";
                    }
                    module.url = folder + "Horarios/Horarios.html";
                    break;
                case "add_horario_alumno":
                    module.excecute = function () {
                        app.module = "Crear mi horario";
                    }
                    module.url = folder + "Horarios/Horarios-alumnos.html";
                    break;
                case "add_mis_rubricas":
                    module.excecute = function () {
                        app.module = "Plan de Ejecución";
                    }
                    module.url = folder + "Plan_Ejecucion/Plan_Ejecucion.html";
                    break;
                case "add_acudiente":
                    module.excecute = function () {
                        app.module = "Acudientes";
                    }
                    module.url = folder + "Acudientes/Acudientes.html";
                    break;
                case "get_calificaciones":
                    module.excecute = function () {
                        app.module = "Mis Calificaciones";
                    }
                    module.url = folder + "Calificaciones/Calificaciones.html";
                    break;
                case "get_calificaciones_alumno":
                    module.excecute = function () {
                        app.module = "Mis Calificaciones";
                    }
                    module.url = folder + "Calificaciones/Calificaciones-alumnos.php";
                    break;
                case "add_horario_acudiente":
                    module.excecute = function () {
                        app.module = "Crear mi horario";
                    }
                    module.url = folder + "Horarios/Horarios-acudiente.html";
                    break;
                case "add_user":
                    module.excecute = function () {
                        app.module = "Crear Usuarios";
                    }
                    module.url = folder + "Usuarios/Usuarios.html";
                    break;
                case "add_grupos":
                    module.excecute = function () {
                        app.module = "Grupos Electivos";
                    }
                    module.url = folder + "Grupos_Electivos/Grupos_Electivos.html";
                    break;
                case "add_perfil":
                    if($("#fi_type").val()=="alumno"){
                        module.excecute = function () {
                            app.module = "Mi Perfil";
                        }
                        module.url = folder + "Perfil/Alumnos.php";
                    }
                    if($("#fi_type").val()=="profesor"){
                        module.excecute = function () {
                            app.module = "Mi Perfil";
                        }
                        module.url = folder + "Perfil/Profesores.php";
                    }
                    if($("#fi_type").val()=="acudiente"){
                        module.excecute = function () {
                            app.module = "Mi Perfil";
                        }
                        module.url = folder + "Perfil/Acudientes.php";
                    }
                    if($("#fi_type").val()=="admin"){
                        module.excecute = function () {
                            app.module = "Mi Perfil";
                        }
                        module.url = folder + "Perfil/Usuarios.php";
                    }
                    break;
            }
            app.loadmodule(module);
        }
    }
});
function expan(view="#vistas"){
    $(view).toggleClass( "card-fullscreen" );
}
function preload_submit() {
    html = '<div id="preload_sutmit" style="z-index:900;position:absolute;top:0px; left:0px; opacity:0.8;background-color:#000;width:100%;height:100%;display: flex;align-items: center;justify-content: center;"><img width="120" src="images/preload.svg"></div>';
    $("body").append(html);
}
function save(data) {
    var defecto = {
        type: "GET"
    };
    $.extend(defecto, data);
    preload_submit();
    $.ajax({
        data: defecto.data,
        type: defecto.type,
        url: defecto.url,
        success: function (result) {
            $("#preload_sutmit").remove();
            console.log(result);
            defecto.result(result);
        },
        error: function (xhr, status, error) {
            console.log(defecto.url);
            console.log(xhr);
            console.log(status);
            console.log(error);
            $("#preload_sutmit").remove();
            swal({
                type: 'error',
                title: 'upsss',
                html: "ha ocurrido un error inesperado al conectarse a internet intentelo mas tarde " + error + " " + status + " " + xhr
            });
        }
    });
}
function session_expired() {
    location.reload();
}
function validar(elements) {
    var mal = 0;
    var html_ul1 = "<ul>";
    var html_ul2 = "</ul>";
    var html = "";
    for (var campos in elements) {
        if (elements[campos].obligatorio) {
            if ($(campos).val() == "") {
                $(campos).css("border", "2px solid red");
                if ($(campos).attr("data-error") != "true") {
                    $(campos).on({
                        click: function () {
                            $(this).css("border", "1px solid #ced4da");
                        }
                    });
                }
                $(campos).attr("data-error", "true");
                mal += 1;
                html += "<li>" + elements[campos].msg + "</li>"
            }
        }
    }
    if (mal == 0) {
        return true;
    } else {
        swal({
            type: 'error',
            title: 'Error en el formulario',
            html: html
        });
        return false;
    }
}
function paginador(page, show, n_results) {
    var items = 7;
    pagination = {
        prev: true,
        next: true,
        going: show,
        pages: []
    };
    var indices = Math.ceil(n_results / show);
    if (page == 1) {
        pagination.prev = false;
    } else {
        pagination.prev = true;
    }
    if (indices == page) {
        pagination.next = false;
    }
    if (indices > items) {
        if (page >= 4) {
            //si esta paginacion y va mas en la mitad del controlador//
            var i = 0;
            var start = page - 3;
            while (i < items) {
                if (indices >= start) {
                    pagination.pages.push(start);
                }
                start++;
                i++;
            }
        } else {
            //si esta comenzando la paginacion//
            for (var i = 1; i <= items; i++) {
                pagination.pages.push(i);
            }
        }
    } else {
        //si los resultados no alcanzan a superar el inidce de 7//
        for (var i = 1; i <= indices; i++) {
            pagination.pages.push(i);
        }
    }
    console.log(pagination.pages + " => Paginas" + show);
    return pagination;
}
var handleFileSelect = function (evt, img, f) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload = function (readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById(img).value = btoa(binaryString);
            f(btoa(binaryString));
            return true;
        };
        reader.readAsBinaryString(file);
    }
}