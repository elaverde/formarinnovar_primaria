<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$act=new Models\Actividades_PDF();
$info=new Models\Info_System();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("POST");
    if($id_archivo==0){
        //si es nuevo regirtro//
        $newname=$info->generate_keys();
        foreach($_FILES['files']['error'] as $key => $error){
           if($error == UPLOAD_ERR_OK){
               $name = $_FILES['files']['name'][$key];
               move_uploaded_file($_FILES['files']['tmp_name'][$key], '../../Actividades/' . $name);
               rename("../../Actividades/".$name, '../../Actividades/'.$newname.'.pdf'  );
               chmod('../../Actividades/'.$newname.'.pdf', 0777);
           }
        }
        $act->__set("plan_id",$plan_id);
        $act->__set("nombre",$nombre);
        $act->__set("url",$newname.".pdf");
        $act->__set("id", null);
        $result->completed = boolval($act->save());
    }else{
        //si el proceso es update se miden dos factores si hay archivo de cambio o no//
        if(file_exists($_FILES['files']['tmp_name'][0])) {
            //eliminamos el archivo para aggregar el nuevo
            unlink('../../Actividades/'.$nombre_archivo);
            foreach($_FILES['files']['error'] as $key => $error){
                if($error == UPLOAD_ERR_OK){
                    $name = $_FILES['files']['name'][$key];
                    move_uploaded_file($_FILES['files']['tmp_name'][$key], '../../Actividades/' . $name);
                    rename("../../Actividades/".$name, '../../Actividades/'.$nombre_archivo  );
                    chmod('../../Actividades/'.$nombre_archivo, 0777);
                }
            }
        }
        //registramos el cambio//
        $act->__set("id", $id_archivo);
        $act->__set("nombre",$nombre);
        $result->completed = boolval($act->update());
        $result->completed = boolval(true);
    }
    echo json_encode($result);
}else{
	$result->completed ="expired";
	echo json_encode($result);
}
?>