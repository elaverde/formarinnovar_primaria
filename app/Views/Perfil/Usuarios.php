<div id="Usuarios" class="card card-outline-info">
    <div class="card-header">
        <h4 class="m-b-0 text-white">{{operation}}</h4>
    </div>
    <div class="card-body">
        <form id="form_Usuarios" name="form_Usuarios" action="#">
            <div class="form-body">
                <!--<h3 class="card-title">Registre su informacion</h3>-->
                <hr>
                <div class="row p-t-20">
                    <div class="col-md-12">


                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Perfil</a>
                                </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Contraseña</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="card-body">
                                        <div id="perfil" >

                                            <div class="form-group">
                                                <div style="display:none" class="form-group">
                                                    <label>ID *</label>
                                                    <input v-model="id" id="id" name="id" type="text" class="form-control form-control-line" placeholder="Ingresa el id aquí">
                                                </div>
                                                <div class="form-group">
                                                    <img id="imgfoto" style="float: right;" width="100" src="images/avatar.svg">

                                                    <label>Foto *</label><br>
                                                    <i style="color:red;" id="status_picture" data-toggle="modal" data-target="#exampleModal" @click="activate_cam" class="icon-user">&nbsp Foto Cámara</i>
                                                    <br>
                                                    <label>Foto Archivo *</label>
                                                    <input name="logo" id="logo" type="hidden">
                                                    <input id="logo2" name="logo2" type="file" class="form-control">
                                                    <input id="foto" name="foto" type="hidden" class="form-control form-control-line" placeholder="Ingresa el foto aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Nombre *</label>
                                                    <input id="nombre" name="nombre" type="text" class="form-control form-control-line" placeholder="Ingresa el nombre aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Apellidos *</label>
                                                    <input id="apellidos" name="apellidos" type="text" class="form-control form-control-line" placeholder="Ingresa el apellidos aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Fecha de nacimiento *</label>
                                                    <input id="fecha_nacimiento" name="fecha_nacimiento" type="date" class="form-control form-control-line" placeholder="Ingresa el fecha de nacimiento aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Identificación *</label>
                                                    <input id="identificacion" name="identificacion" type="text" class="form-control form-control-line" placeholder="Ingresa el identificación aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Sexo *</label>
                                                    <select id="sexo" name="sexo" type="text" class="form-control form-control-line">
                                                        <option value="f">Femenino</option>
                                                        <option value="m">Masculino</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Dirección *</label>
                                                    <input id="direccion" name="direccion" type="text" class="form-control form-control-line" placeholder="Ingresa el dirección aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Correo electrónico *</label>
                                                    <input id="email" name="email" type="text" class="form-control form-control-line" placeholder="Ingresa el correo electrónico aquí">
                                                </div>
                                                <div id="passwordbox" class="form-group">
                                                    <label>Password *</label>
                                                    <input id="password" name="password" type="text" class="form-control form-control-line" placeholder="Ingresa el password aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Teléfono *</label>
                                                    <input id="telefono" name="telefono" type="text" class="form-control form-control-line" placeholder="Ingresa el teléfono aquí">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de Usuario *</label>
                                                    <select id="tipo_rol" name="tipo_rol" class="form-control form-control-line">
                                                        <option value="Admin">Administrador</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de sangre *</label>
                                                    <select id="tipo_sangre" name="tipo_sangre" class="form-control form-control-line">
                                                        <option value="A+">A+</option>
                                                        <option value="A-">A-</option>
                                                        <option value="B+">B+</option>
                                                        <option value="B-">B-</option>
                                                        <option value="O+">O+</option>
                                                        <option value="O-">O-</option>
                                                        <option value="AB+">AB+</option>
                                                        <option value="AB-">AB-</option>
                                                    </select>
                                                </div>

                                                <div class="button-box m-t-20">
                                                    <a v-if="id==0" @click="onsave" id="add-option" class="btn btn-success" href="#">Agregar</a>
                                                    <a v-if="id!=0" @click="onsave" id="add-option" class="btn btn-success" href="#">Modificar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--second tab-->
                                <div class="tab-pane" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Contraseña *</label>
                                            <input id="password1" name="password1" type="password" class="form-control form-control-line" placeholder="Ingresa el nombre aquí">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmar Contraseña *</label>
                                            <input id="password2" name="password2" type="password" class="form-control form-control-line" placeholder="Ingresa el apellidos aquí">
                                        </div>
                                        <div class="button-box m-t-20">
                                            <a @click="onupdatepassword" id="add-option" class="btn btn-success" href="#update">Guardar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </div>


                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Fotografia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="contenedor">
                    <div class="titulo">Cámara</div>
                    <iframe id="web-cam" name="web-cam" frameborder="0" width="100%" height="300"></iframe>
                </div>

            </div>
            <div class="modal-footer">
                <button id='botonFoto' data-dismiss="modal" type="button" class="btn btn-primary">Tomar</button>
            </div>
        </div>
    </div>
</div>
</div>

<script src="Views/Perfil/Usuarios.js"></script> 