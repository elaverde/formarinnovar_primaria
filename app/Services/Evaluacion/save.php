<?php
header('Content-Type: application/json');
date_default_timezone_set("America/Bogota");
require_once "../../Config/Autoload.php";
Config\Autoload::run();
$db= new Models\Conexion();
$eva=new Models\Evaluacion();
$info=new Models\Info_System();
$eje=new Models\Ejecucion();
$result=  new stdClass();
if($info->session_active()){
    $db->extract_dates("GET");
    $index=0;
    foreach ( $_GET["id"] as $dni ) { 
        $eva->__set("id",$dni);
        $eva->__set("dia", date("Y-m-d H:i:s"));
        $eva->__set("profesor", $_SESSION['fi_id']);
        $eva->__set("nota", $_GET["notas"][$index]);
        $eva->__set("comentario", $_GET["comment"][$index]);
        $eva->update();
        $index++;
    }

    $eje->__set("promediorubrica",$notafinal);
    $eje->__set("id",$ejecucion_plan_id);
    $eje->update();

    $result->completed =true;
    echo json_encode($result);
}else{
    $result->completed ="expired";
    echo json_encode($result);
}
?>