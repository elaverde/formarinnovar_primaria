<?php
/**
 * Created by PhpStorm.
 * User: elaverde
 * Date: 21/8/2018
 * Time: 23:02
 */

namespace Models;


class Info_System
{
    private $email="soporte@formarinnovar.com";
    private $pass="unlugarparaserfeliz";
    private $from="Soporte Formar Innovar";
    public function session_active(){
        if (isset($_SESSION['fi_id'])){
            return true;
        }else{
            return false;
        }
    }
    public function get_session($id){
        return $_SESSION[$id];
    }
    public function generate_keys($longitud = 10, $opcLetra = TRUE, $opcNumero = TRUE, $opcMayus = TRUE, $opcEspecial = FALSE){
        $letras ="abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $especiales ="|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == TRUE) $listado .= $letras;
        if ($opcNumero == TRUE) $listado .= $numeros;
        if($opcMayus == TRUE) $listado .= $letrasMayus;
        if($opcEspecial == TRUE) $listado .= $especiales;

        for( $i=1; $i<=$longitud; $i++) {
            $caracter = $listado[rand(0,strlen($listado)-1)];
            $password.=$caracter;
            $listado = str_shuffle($listado);
        }
        return $password;
    }
    public function get_type_user($id){
        if($id==$_SESSION["fi_type"]){
            return true;
        }else{
            return false;
        }
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }
}
