elements = {
	"#idalumno": {
		"obligatorio": true,
		"msg": "Por favor seleccioné el alumno"
	}
}
var Matriculas = new Vue({
    el: '#Matriculas',
    data: {
        id: 0,
        operation: "Matrícular",
        list: null,
        preload_list: false,
        prev: false,
        next: false,
        pages: null,
        results: 0,
        page: 1,
        show: 0,
        namealum: "Nombre",
        dnialum: "Identificación",
        optionsSelect1: [{
            text: "Cargando..",
            value: "0"
        }]
    },
    created: function () {
        this.onListGrados();
        this.onlistAlumnos(1);
    },
    methods: {
        onListGrados: function () {
            var url = 'Services/Grados/select.php';
            var self = this;
            $.get(url, function (data) {
                datos = data.data;
                self.optionsSelect1 = [];
                for (var i = 0; i < datos.length; i++) {
                    self.optionsSelect1.push({
                        text: datos[i].nombre,
                        value: datos[i].id
                    });
                }
            });
        },
        onlistAlumnos: function (page) {
            var self = this;
            self.preload_list = true;
            if (page == "next") {
                page = parseInt(self.page) + 1
            };
            if (page == "prev") {
                page = parseInt(self.page) - 1
            };
            var url = 'Services/Matriculas/list_alumnos.php?paginas=15&page=' + page + '&nombre=' + $('#search').val() + '&apellidos=' + $('#search').val() + '&identificacion=' + $('#search').val();
            $.get(url, function (data) {
                if (data.completed == "expired") {
                    session_expired();
                } else {
                    self.list = data["data"];
                    self.results = data.result;
                    self.page = Math.ceil(data.page / 15);
                    self.show = data.show;
                    var tab = paginador(self.page, self.show, self.results);
                    self.prev = tab.prev;
                    self.next = tab.next;
                    self.pages = tab.pages;
                    self.preload_list = false;
                }
            });
        },
        onmatricular: function (item) {
            $("#idalumno").val(item.id);
            this.namealum = "Nombres: " + item.nombre + " " + item.apellidos;
            this.dnialum = "Identificación: " + item.identificacion;
            $("#avatar_alumno").attr("src", item.foto);
        },
        onsave: function () {
            var self = this;
            if (validar(elements)) {
                save({
                    url: "Services/Matriculas/save.php?" + $("#form_Matriculas").serialize(),
                    result: function (data) {
                        if (data.completed) {
                            swal({
                                type: 'success',
                                title: 'La matricula se ha realizado correctamente',
                                showConfirmButton: false,
                                timer: 3500
                            });
                        }
                        if (data.completed == "expired") {
                            session_expired();
                        }
                        self.id = 0;
                        self.namealum="";
                        self.dnialum="";
                        self.onlistAlumnos(1);
                        $("#avatar_alumno").attr("src", "assets/images/users/avatar.svg");
                        $("#idalumno").val("");
                    }
                });
            }
        }
    }
});