<?php

namespace Models;
class Profesores
{
	private $id;
	private $foto;
	private $nombre;
	private $apellidos;
	private $fecha_nacimiento;
	private $identificacion;
	private $sexo;
	private $direccion;
	private $password;
	private $telefono;
	private $tipo_sangre;
	private $email;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    public function exist(){
        $sql="SELECT * FROM profesores WHERE identificacion=:identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
            ':identificacion'=>$this->__get("identificacion")
        ));
        if(count($resultado)==0){
            return  false;
        }else{
            return  true;
        }
    }
    public function save(){
        $sql="INSERT INTO profesores (id, foto, nombre, apellidos, fecha_nacimiento, identificacion, sexo, direccion, password, telefono, tipo_sangre, email) VALUES (:id, :foto, :nombre, :apellidos, :fecha_nacimiento, :identificacion, :sexo, :direccion, :password, :telefono, :tipo_sangre, :email)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':password'=>$this->__get('password'),
			':telefono'=>$this->__get('telefono'),
			':tipo_sangre'=>$this->__get('tipo_sangre'),
			':email'=>$this->__get('email')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE profesores SET foto= :foto, nombre= :nombre, apellidos= :apellidos, fecha_nacimiento= :fecha_nacimiento, identificacion= :identificacion, sexo= :sexo, direccion= :direccion, telefono= :telefono, tipo_sangre= :tipo_sangre, email= :email WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':foto'=>$this->__get('foto'),
			':nombre'=>$this->__get('nombre'),
			':apellidos'=>$this->__get('apellidos'),
			':fecha_nacimiento'=>$this->__get('fecha_nacimiento'),
			':identificacion'=>$this->__get('identificacion'),
			':sexo'=>$this->__get('sexo'),
			':direccion'=>$this->__get('direccion'),
			':telefono'=>$this->__get('telefono'),
			':tipo_sangre'=>$this->__get('tipo_sangre'),
			':email'=>$this->__get('email')
        ));
        return $resultado;
    }
    public function update_password(){
        $sql="UPDATE profesores SET password = :password WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function update_password_automatic(){
        $sql="UPDATE profesores SET password = :password WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':identificacion'=>$this->__get('identificacion'),
			':password'=>$this->__get('password')
        ));
        return $resultado;
    }
    public function get_user_by_dni(){
        $sql="SELECT email,nombre,apellidos,identificacion FROM profesores WHERE identificacion = :identificacion";
        $resultado = $this->con->ejecutarConsulta($sql,array(
			':identificacion'=>$this->__get('identificacion')
        ));
        $user->email=$resultado[0]["email"];
        $user->nombre=$resultado[0]["nombre"];
        $user->apellidos=$resultado[0]["apellidos"];
        $user->identificacion=$resultado[0]["identificacion"];
        return $user;
    }
    public function delete(){
        $sql="DELETE FROM profesores WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function get_profesor(){
        $sql="SELECT * FROM profesores WHERE id=:id";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':id'=>$this->__get('id')
        ));
        return $resultado1;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM profesores WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM profesores WHERE nombre LIKE :nombre OR apellidos LIKE :apellidos OR  identificacion LIKE :identificacion ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':apellidos'=>'%'.$this->__get('apellidos').'%',
			':identificacion'=>'%'.$this->__get('identificacion').'%'
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}