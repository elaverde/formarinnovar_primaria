<?php

namespace Models;
class Grupos_Electivos
{
	private $id;
	private $grado_id;
	private $nombre;

    private $con;
    public function __construct()
    {
        $this->con = new Conexion();
    }
    
    public function save(){
        $sql="INSERT INTO grupos_electivos (id, grado_id, nombre) VALUES (:id, :grado_id, :nombre)";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':grado_id'=>$this->__get('grado_id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function update(){
        $sql="UPDATE grupos_electivos SET grado_id= :grado_id, nombre= :nombre WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
			':id'=>$this->__get('id'),
			':grado_id'=>$this->__get('grado_id'),
			':nombre'=>$this->__get('nombre')
        ));
        return $resultado;
    }
    public function delete(){
        $sql="DELETE FROM grupos_electivos WHERE id = :id";
        $resultado = $this->con->ejecutarActualizacion($sql,array(
            ':id'=>$this->__get("id")
        ));
        return $resultado;
    }
    public function list($start,$finish){
        $result=  new  \stdClass();
        if($start==1){
            $start=0;
        }else{
            $start=((($start-1)*$finish));
        }
        //generamos la consulta
        $sql="SELECT * FROM grupos_electivos WHERE  grado_id = :grado_id and nombre LIKE :nombre LIMIT $start, $finish";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':nombre'=>'%'.$this->__get('nombre').'%',
			':grado_id'=>$this->__get('grado_id')
        ));
        //buscamos los n resultados
        $result->data=$resultado1;
        $sql="SELECT  count(*) as results FROM grupos_electivos WHERE   grado_id = :grado_id and nombre LIKE :nombre ";
        $resultado2 = $this->con->ejecutarConsulta($sql,array(
            ':nombre'=>'%'.$this->__get('nombre').'%',
            ':grado_id'=>$this->__get('grado_id')
        ));
        //se lo asignamos a la respuesta
        $result->result=$resultado2[0]["results"];
        //los resultados a mostrar
        $result->show=$finish;
        $result->page=$start+1;

        return $result;
    }
    public function list_grupo_select(){
        //generamos la consulta
        $sql="SELECT * FROM grupos_electivos WHERE  grado_id = :grado_id ";
        $resultado1 = $this->con->ejecutarConsulta($sql,array(
			':grado_id'=>$this->__get('grado_id')
        ));
        return $resultado1;
    }
    public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
    }
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
    }
    
}